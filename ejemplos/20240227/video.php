<?php
//estamos cargando la clase en el espacio de nombre actual
use clases\Aplicacion;
use clases\Header;
use clases\Modelo;
use clases\Pagina;

//cargamos la clase
require 'autoload.php';

$aplicacion =new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query('select * from favoritos where categorias ="video"');
$cabecera =  Header::ejecutar([
    'titulo'=>'Videos',
    'subtitulo'=>$aplicacion->configuraciones['autor'],
    'salida'=>"Pagina realtivas a videos"
]);
Pagina::comenzar();
?>

<div class="container">
    <?=$favoritos->gridViewBotones()?>
</div>
<?php
Pagina::terminar([
    "titulo"=>"Video",
    "cabecera"=>$cabecera,
    "pie"=>"Creado por ".$aplicacion->configuraciones['autor']
]);
