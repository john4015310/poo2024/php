<?php

// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\Header;
use clases\Modelo;
use clases\Pagina;
use clases\GridView;


// definiendo el autoload
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});
//inicializo mensaje
$mensaje = "";

// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
//le indico que campos voy a insertar 
$favoritos->setCampos(["url","titulo","descripcion","categorias"]);
//le indico al modelo en donde tiene que insertar el registro
$favoritos->tabla="favoritos";

// compruebo si me llegan datos del post
if ($_POST) {
    // asigno los datos a un array asociativo
    $favoritos->setDatos($_POST)->save();
    $mensaje = "<div class='alert alert-success'>El registro se ha insertado correctamente</div>";
}


$cabecera = Header::ejecutar([
    "titulo" => "Pagina de insercion",
    "subtitulo" => $aplicacion->configuraciones['autor'],
    "salida" => "Aplicacion para mostrar paginas interesantes"
]);

Pagina::comenzar();
?>
<?php

//al formulario le paso los datos y la accion para que ponga insertar
if(!$_POST){
    $datos= $favoritos->datos;
    $accion = "insertar";
    require "_form.php";
}else{
    echo $mensaje;
}

?>


<?php
Pagina::terminar([
    "titulo" => "inicio",
    "cabecera" => $cabecera,
    "pie" => "Creado por: " . $aplicacion->configuraciones['autor']
]);


