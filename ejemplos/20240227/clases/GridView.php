<?php
namespace clases;

use mysqli_result;

require 'autoload.php';
class GridView{
    public static function render(mysqli_result $resultados, array $botones=[], array $campos=[]): string
    {
            if ($resultados->num_rows > 0) {
                $registros = $resultados->fetch_all(MYSQLI_ASSOC);
                // mostrar los registros
                $salida = "<table class='table table-striped table-bordered'>";
                $salida .= "<thead class='table-dark'><tr>";
                //compruebo si el usuario me ha pasado los datos a mostrar
                if(count($campos)==0){
                    //si no me ha pasado los campos los leo del primer registro de la base de datos
                    $campos = array_keys($registros[0]);
                }
               
                foreach ($campos as $campo) {
                    $salida .= "<th>$campo</th>";
                }
    
                // añado una columna para los botones
                if(count($botones)>0){
                    $salida .= "<td>Acciones</td>";
                }
                
                // foreach($registros[0] as $campo=>$valor){
                //     $salida .= "<td>$campo</td>";
                // }
                $salida .= "</tr></thead>";
                // muestro todos los registros
                foreach ($registros as $registro) {
                    $salida .= "<tr>";
                    // mostrando los campos que hemos selecionado desde los argumentos de la funsion
                    foreach ($campos as $campo) {
                        $salida .= "<td>" . $registro[$campo] . "</td>";
                    }
                    // mostrando los botones
                    if(count($botones)>0){
                        $salida .= "<td>";
    
                        foreach ($botones as $label => $enlace) {
                            $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> ";
                    }
                    
    
                    // cerramos la celda
                    $salida .= "</td>";
                    }
                    
    
                    $salida .= "</tr>";
                }
                $salida .= "</table>";
            } else {
                $salida = "No hay registros";
            }
            //colocamos el puntero de la consulta al primer registro
            $resultados->data_seek(0);
            return $salida;
    }
}