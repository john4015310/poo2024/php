<?php
namespace clases;
class Header extends Widget{
    public static function ejecutar(array $parametros=[],string $layout='componentes/jumbotron'):string{
        return parent::ejecutar($parametros,$layout);
    }
    public static function terminar( array $parametros=[],string $layout='componentes/jumbotron'):void{
        //termina el flujo de salida
        // para que nos muestre el html
        //en el navegador
        $contenido =  ob_get_clean();
        //conviertir array asociativo a métodos en variables
        //para poder usarlos en el html
        extract($parametros);
        require 'layout/'.$layout.'.php';

    }
}