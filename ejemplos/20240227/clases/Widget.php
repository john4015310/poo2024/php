<?php
namespace clases;
class Widget{
    public static function comenzar(){
        //contrala el flujo de salida
        //quiere decir todo lo que tengas que enseñar se queda cacheado e el ordenador
        ob_start();
    }

    public static function ejecutar(array $parametros=[],string $layout='layout1'):string{
        extract($parametros);
        ob_start();
        require 'layout/'.$layout.'.php';
        return ob_get_clean();
    }

    /**
     * metodo para mostrar la pagina con un determinado layout
     *
     * @param string $titulo el titulo a mostrar de la pagina
     * @param string $layout el nombre del layout a utilizar
     * @return string
     */

    public static function terminar(array $parametros=[],string $layout='layout1'):void{
        //termina el flujo de salida
        // para que nos muestre el html
        //en el navegador
        $contenido =  ob_get_clean();
        //conviertir array asociativo a parametros en variables 
        //para poder usarlos en el html
        extract($parametros);
        require 'layout/'.$layout.'.php';
    }

}