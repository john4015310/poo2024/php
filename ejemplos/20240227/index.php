<?php
//estamos cargando la clase en el espacio de nombre actual
use clases\Aplicacion;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;

//cargamos la clase
require 'autoload.php';

$aplicacion =new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$query= $favoritos->query("select * from favoritos");
$cabecera =  Header::ejecutar([
    'titulo'=>'Pagina de Inicio',
    'subtitulo'=>$aplicacion->configuraciones['autor'],
    'salida'=>"Aplicacion para mostrar paginas interesantes"
]);
Pagina::comenzar();
?>

<div class="container">
    <!-- //comentado -->
    <!-- <?=$favoritos->gridViewBotones([],["id","titulo","url"])?> -->
    <!-- //terminado -->

    <?=GridView::render($query,["Mostrar <i class='fa-solid fa-eye'></i>"=>"ver.php"],["id","titulo","url"])?>
    <!-- [
        "editar <i class='fa-solid fa-pen-to-square'></i>"=>"editar.php",
        "borrar <i class='fa-solid fa-trash'></i>"=>"borrar.php"
    ] -->
</div>
<?php
Pagina::terminar([
    "titulo"=>"Inicio",
    "cabecera"=>$cabecera,
    "pie"=>"Creado por ".$aplicacion->configuraciones['autor']
]);
