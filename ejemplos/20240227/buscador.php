<?php
//estamos cargando la clase en el espacio de nombre actual
use clases\Aplicacion;
use clases\Header;
use clases\Modelo;
use clases\Pagina;

//cargamos la clase
require 'autoload.php';

$aplicacion =new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
$favoritos->query('select * from favoritos where categorias ="buscador"');
$cabecera =  Header::ejecutar([
    'titulo'=>'Buscador',
    'subtitulo'=>$aplicacion->configuraciones['autor'],
    'salida'=>"Buscador"
]);
Pagina::comenzar();
?>

<div class="container">
    <?=$favoritos->gridViewBotones()?>
</div>
<?php
Pagina::terminar([
    "titulo"=>"Buscador",
    "cabecera"=>$cabecera,
    "pie"=>"Creado por ".$aplicacion->configuraciones['autor']
]);
