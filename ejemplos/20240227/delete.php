<?php
// cargando la clase en el espacio de nombres actual
use clases\Aplicacion;
use clases\Modelo;

// cargando la clase en el espacio de nombres actual
require ("autoload.php");

// instanciando la clase
$aplicacion = new Aplicacion();
$favoritos = new Modelo($aplicacion->db);
// le pido que elimine el registro con id
