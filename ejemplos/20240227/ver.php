<?php
//estamos cargando la clase en el espacio de nombre actual
use clases\Aplicacion;
use clases\DetailView;
use clases\GridView;
use clases\Header;
use clases\Modelo;
use clases\Pagina;

//cargamos la clase
require 'autoload.php';

//creo un objeto de tipo aplicacion
$aplicacion =new Aplicacion();
//creo un objeto de tipo mysqli es una conexion con la bbdd
$favoritos = new Modelo($aplicacion->db);

$_GET['id']= $_GET['id'] ?? 0;
$query = $favoritos->query("select * from favoritos where id =". $_GET['id']);

$cabecera =  Header::ejecutar([
    'titulo'=>'Detalles de la pagina',
    'subtitulo'=>$aplicacion->configuraciones['autor'],
    'salida'=> $_GET['id']
]);
Pagina::comenzar();
if ($query->num_rows == 0) {
    DetailView::comenzar();

?>
<li class="list-group-item"> No tenemos ningun registro</li>
<?php
    DetailView::terminar(["titulo" => "Aviso"]);
} else {
    echo DetailView::ejecutar([
        "query"=>$query,
        "campos"=>["id","url","titulo","descripcion","categoria"],
        "campoTitulo"=>"titulo"
    ]);
}
  
?>
    <!-- [
        "editar <i class='fa-solid fa-pen-to-square'></i>"=>"editar.php",
        "borrar <i class='fa-solid fa-trash'></i>"=>"borrar.php"
    ] -->
</div>
<?php
Pagina::terminar([
    "titulo"=>"Inicio",
    "cabecera"=>$cabecera,
    "pie"=>"Creado por ".$aplicacion->configuraciones['autor']
]);
