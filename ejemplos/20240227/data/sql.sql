﻿DROP DATABASE IF EXISTS aplicacionFavoritos;
CREATE DATABASE aplicacionFavoritos;
USE aplicacionFavoritos;

CREATE TABLE favoritos(
  id int AUTO_INCREMENT,
  url varchar(300),
  titulo varchar(200),
  descripcion text,
  categorias varchar(200),
  imagen varchar(200),
  PRIMARY KEY(id)
);

INSERT INTO favoritos (url, titulo, descripcion, categorias)
  VALUES 
( 'alpeformacion.es', 'Pagina Alpe', 'Pagina de Empresa','formacion'),
('formacion.es','Pagina de Formacion', 'Pagina de la Empresa','formacion'),
('google.es','Pagina de Google', 'Pagina de la Empresa','buscador'),
('youtube.es','Pagina de Youtube', 'Pagina de la Empresa','video');


select * from favoritos where categorias ='formacion';