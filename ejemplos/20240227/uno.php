<?php
use clases\Pagina;
use clases\Aplicacion;
use clases\Header;

require 'autoload.php';
$aplicacion =new Aplicacion();
$cabecera =  Header::ejecutar([
    'titulo'=>'Pagina uno',
    'subtitulo'=>$aplicacion->configuraciones['autor'],
    'salida'=>"Aplicacion para mostrar paginas interesantes"
]);
Pagina::comenzar();
?>
<div class="container">
    <div class="row my-3">
        <ul class="list-group">
            <li class="list-group-item">1</li>
            <li class="list-group-item">2</li>
            <li class="list-group-item">3</li>
            <li class="list-group-item">4</li>
            <li class="list-group-item">5</li>
        </ul>
    </div>
</div>

<?php
Pagina::terminar([
    'titulo'=>'Pagina de Inicio',
    'cabecera'=>$cabecera,
    "pie"=>"Creado por ".$aplicacion->configuraciones['autor']
]);