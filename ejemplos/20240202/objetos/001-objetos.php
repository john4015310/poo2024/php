<?php

class Padre{

    public string $nombre;
    public string $apellidos;
    public string $tipo;

    private int $edad;
    protected int $altura;

    public int $nivel;

    public function __construct(string $nombre, string $apellidos,  int $edad, int $altura,string $tipo){

        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->tipo = $tipo;
        $this->edad = $edad;
        $this->altura = $altura;
        $this->nivel = 1;
    }

    //getter y setter

    public function getEdad(){
        return $this->edad;
    }

    public function getTipo()
    {
        return $this->tipo;
    }


    //metodos
    public function presentarme(){
        return "Soy el padre";
    }

    public function asignar($nombre, $apellidos){
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
    }
}

class Hijo extends Padre{
    //propiedad esta sobreescrita
    public int $nivel;
    //la propiedad edad que es privada la sobreescribo
    public int $edad;

    //sobreescritua de metodos
    public function __construct(string $nombre, string $apellidos, int $edad, int $altura){
        parent::__construct($nombre, $apellidos, $edad, $altura,"padre");
        $this->tipo = "hijo";
        $this->edad = $edad;
    }

    //sobreescritura de metodos
    // public function getEdad(){
    //     return $this->edad;
    // }
    public function presentarme(){
        return "Soy el hijo";
    }
}


$padre1 = new Padre("Juan", "Lopez", 25, 175, "padre");
echo $padre1->presentarme();

$hijo1 = new Hijo("Pedro", "Garcia", 5, 175);

var_dump($padre1);
var_dump($hijo1);


echo $padre1->getEdad();
echo "<br>";
echo $hijo1->getTipo();