<?php
namespace src;
class Padre{
    
    public string $nombre;
    public string $apellidos;
    public string $tipo;

    private int $edad;
    protected int $altura;

    public int $nivel;

    public function __construct(string $nombre, string $apellidos,  int $edad, int $altura,string $tipo){

        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->tipo = $tipo;
        $this->edad = $edad;
        $this->altura = $altura;
        $this->nivel = 1;
    }

    //getter y setter

    public function getEdad(){
        return $this->edad;
    }

    public function getTipo()
    {
        return $this->tipo;
    }


    //metodos
    public function presentarme(){
        return "Soy el padre";
    }

    public function asignar($nombre, $apellidos){
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
    }
}