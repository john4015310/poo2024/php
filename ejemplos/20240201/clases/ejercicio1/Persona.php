<?php
namespace Clases\ejercicio1;

class Persona{
    public string $nombre;
    public int $edad;

    public function __construct(string $nombre, int $edad){
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public function mostrarInformacion(){
        $mostrar = "<h2>Información de la persona</h2>";
        $mostrar .= "<p>Nombre: " . $this->nombre . "</p>";
        $mostrar .= "<p>Edad: " . $this->edad . "</p>";

        return $mostrar;
    }
}