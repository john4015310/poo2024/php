<?php
namespace Clases\ejercicio2;

class Producto{
    public int $idProducto;
    public string $nombre;
    public float $precio;
    public int $stock;
    public string $descripcion;

    public function __construct(int $idProducto, string $nombre, float $precio, int $stock, string $descripcion) {
        $this->idProducto = $idProducto;
        $this->nombre = $nombre;
        $this->precio = $precio;
        $this->stock = $stock;
        $this->descripcion = $descripcion;
    }
    
    public function mostrarDetalles():string{
        $mostrar= "<hr>";
        $mostrar .= "<h2>Detalles del producto</h2>";
        $mostrar .= "<p>Id de producto: " . $this->idProducto . "</p>";
        $mostrar .= "<p>Nombre: " . $this->nombre . "</p>";
        $mostrar .= "<p>Precio: " . $this->precio . "</p>";
        $mostrar .= "<p>Stock: " . $this->stock . "</p>";
        $mostrar .= "<p>Descripción: " . $this->descripcion . "</p>";
        
        return $mostrar;
    }

    public function actualizarStock(int $nuevoStock):self{
        $this->stock = $nuevoStock;
        return $this;
    }

    public function calcularPrecioDescuento(int $porcentaje):float{
        $this->precio =  $this->precio - ($this->precio * $porcentaje / 100);
        return $this->precio;
    }

}