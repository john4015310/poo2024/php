<?php
namespace Clases\ejercicio2;

class Usuario{
    public int $idUsuario; 
    public string $nombre;
    public string $email;
    public int $edad;
    public bool $activo;

    public function __construct(int $idUsuario, string $nombre, string $email, int $edad, bool $activo){
        $this->idUsuario = $idUsuario;
        $this->nombre = $nombre;
        $this->email = $email;
        $this->edad = $edad;
        $this->activo = $activo;
    }
    

    public function mostrarInformacion(){
        $mostrar = "<hr>";
        $mostrar .= "<h2>Información de usuario</h2>";
        $mostrar .= "<p>Id de usuario: " . $this->idUsuario . "</p>";
        $mostrar .= "<p>Nombre: " . $this->nombre . "</p>";
        $mostrar .= "<p>Email: " . $this->email . "</p>";
        $mostrar .= "<p>Edad: " . $this->edad . "</p>";
        $mostrar .= "<p>Activo: " . $this->activo . "</p>";
        $mostrar .= "<hr>";
        return $mostrar;
    }

    public function activarUsuario(): void{
        $this->activo = true;
    }

    public function cambiarEdad(int $nuevaEdad):self{
        $this->edad = $nuevaEdad;
        return $this;
    }
}