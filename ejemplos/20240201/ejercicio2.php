<?php
use clases\ejercicio2\Producto;
use clases\ejercicio2\Usuario;
include 'autoload.php';

$usuario1= new Usuario(1,"Juan","juan@yii.com",25,false);
$usuario2= new Usuario(2,"John","babayaga@yii.com",30,true);

$producto1 = new Producto(1,"Pantalon", 1000, 5, "Pantalon de mez");
$producto2 = new Producto(2,"Camisa", 500, 10, "Camisa de verano");

echo $usuario1->mostrarInformacion();
echo $usuario2->mostrarInformacion();

$usuario2->cambiarEdad(37);
echo $usuario2->mostrarInformacion();

echo $producto1->mostrarDetalles();

echo $producto1->calcularPrecioDescuento(10);

echo $producto1->mostrarDetalles();



