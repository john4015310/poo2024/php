<?php
spl_autoload_register(function ($clase) {
    require "clases/". $clase . '.php';
});

//instanciar un perro

$perro1 = new Perro();
$perro1->nombre = "firulais";

//instanciar un segundo perro
$perro2 = new Perro("Thor");

var_dump($perro1, $perro2);