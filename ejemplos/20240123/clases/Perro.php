<?php
class Perro
{
    public $nombre;

    public function ladrar(){
        return "Guau";
    }

    //metodo constructor
    //es recomendable utilizarle para inicializar el objeto
    public function __construct($nombre = ""){
        $this->nombre = $nombre;
    }

}