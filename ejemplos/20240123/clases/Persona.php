<?php

class Persona{
    //Atributos
    public $nombre;
    public $apellidos;
    public $direccion;
    public $localidad;
    public $cp;
    //Metodos
    public function hablar(){
        return " ";
    }

    public function direccion(){
        echo $this->direccion.", ".$this->localidad.", ".$this->cp;
    }

    //metodo que se ejecuta automaticamente cuando 
    //
    public function __construct(){
        //inicializo atributos

        $this->nombre = "";
        $this->apellidos = "";
        $this->direccion = "";
        $this->localidad = "";
        $this->cp = "";
    }
}