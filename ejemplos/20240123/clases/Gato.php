<?php
class Gato
{
    //atributos
    public $nombre;
    public $color;  
    public $pelo;
    public $peso;

    //metodo constructor
    public function __construct($nombre="", $color="negro", $pelo="si", $peso=5) {
        $this->nombre = $nombre;
        $this->color = $color;
        $this->pelo = $pelo;
        $this->peso = $peso;
    }

    //metodos
    public function maulla(){
        echo "Miau";
    }
}