<?php
    class Persona {
        //definir los miembros de mi clase

        //atributos de la clase
        public $nombre="";
        public $apellidos="";
        public $edad=0;

        //metodos de la clase
        public function hablar() {
            //mediante la palabra reserva $his podemos acceder a los miembros de la clase
            return "Soy " . $this -> nombre . " y bla bla bla <br>";    
        }
    }