<?php
//precargar clases
spl_autoload_register(function($clase){
    require "clases/". $clase . '.php';
});

//quiero crear un gato

$gato1 = new Gato();
//le cambio el peso que tenia por defecto
$gato1->peso=6;
//creo otro gato con nombre color y sin pelo
//como no lepaso el peso me coge los 5 por defecto
$gato2 = new Gato("Ares","Blanco","no");

var_dump($gato1, $gato2);

