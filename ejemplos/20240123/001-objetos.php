<?php
    class Persona {
        //definir los miembros de mi clase

        //atributos de la clase
        public $nombre="";
        public $apellidos="";
        public $edad=0;

        //metodos de la clase
        public function hablar() {
            //mediante la palabra reserva $his podemos acceder a los miembros de la clase
            return "Soy " . $this -> nombre . " y bla bla bla <br>";    
        }
    }
//instanciar una clase
//creando un objeto de tipo Persona
    $persona1 = new Persona();
    $persona2 = new Persona();

    //asignando valores  los atributos de la primera persona 
    $persona1 -> nombre = "John";
    $persona1 -> apellidos = "Wick";
    $persona1 -> edad = 25;

    //asignando valores  los atributos de la segunda persona
    $persona2 -> nombre = "Juan";
    $persona2 -> apellidos = "Lopez";
    $persona2 -> edad = 30;

    ///leer los atributos de la primera persona
    echo $persona1 -> nombre . " " . $persona1 -> apellidos . " " . $persona1 -> edad . " "  . "<br>";
    echo "Los apellidos de la primera persona son: " . $persona1 -> apellidos . "<br>";

    //acceder al metodo de la primera persona
    echo $persona1 -> hablar();
    echo $persona2 ->hablar();

    var_dump($persona1);
    var_dump($persona2);
  