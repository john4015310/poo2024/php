<form method="post">
    <div class="row mb-3">
        <label for="mensaje" class="col-sm-3 col-form-label">Mensaje</label>
        <div class="col-sm-9">
            <textarea class="form-control" name="mensaje" id="mensaje" cols="30" rows="10"></textarea>
        </div>
        
    </div>
    <div class="row mb-3">
        <div class="col-sm-9 offset-sm-3">
            <button class="btn btn-primary">Enviar</button>
        </div>
        
    </div>
</form>