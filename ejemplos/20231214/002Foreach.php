<?php
//tenemos un array bidimensional
//es un array enumerados de arrays asociativos
    $datos=[
        [
            'id'=>1,
            'nombre'=>'Juan',
            'apellidos'=>'Perez',
        ],
        [
            'id'=>2,
            'nombre'=>'Maria',
            'apellidos'=>'Lopez',

        ],
        [
            'id'=>3,
            'nombre'=>'Pedro',
            'apellidos'=>'Garcia',
        ]
    ];
    
    //quiero que muestre el array datos en una tabla
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <title>Document</title>
    </head>
    <body>
        <table>
            <thead>
                <?php
                    foreach($datos[0] as $key => $value) {
                        echo "<th>{$key}</th>";
                    }
                ?>
            </thead>
            <tbody>
                <?php
                    foreach($datos as $dato) {
                        echo "<tr>";
                        foreach($dato as $key => $value) {
                            echo "<td>{$value}</td>";
                        }
                        echo "</tr>";
                    }
                ?>
            </tbody>
            
        </table>
    </body>
    </html>