<?php
    //crear un funcion de php que me permita conocer el numero de veces que el primer argumento
    //se repite en el segtundo que es un array

         /**
      * Counts the number of occurrences of a given value in an array.
      *
      * @param mixed $valor The value to count occurrences of.
      * @param array $vector The array to search for occurrences in.
      * @return int The number of occurrences of the value in the array.
      */
    
    $numeros=[1,2,3,4,5,2,3,1,2,3,1];

     function repite($valor,array $vector):int{
        $repet=0;
        for($i=0;$i<count($vector);$i++){
            if($vector[$i]==$valor){
                $repet++;
            }
        }
        return $repet;
        
     }
     //crear un funcion que me permite conocer el numero de vocales
    function contarVocales(array $vector):int{
        $contador=0;
        for($i=0;$i<count($vector);$i++){
            if($vector[$i]=="a"){
                $contador++;
            }
        }
        return $contador;
    } 
?>