<?php
use clases\Personasconstructor;
    require_once "autoload.php";
    require_once "funciones.php";
    require_once "controlErrores.php";
//objeto mysqli
    $conexion = new mysqli("localhost","root","","personas1");
//objeto mysqli_result
    $resultados = $conexion->query("SELECT * FROM personas");

// Array asociativo con el primer registro    
    $registro = $resultados->fetch_assoc();

    //si quiero leer el nombre
    echo $registro["nombre"] . "<br/>";

    var_dump($registro);
// objeto de tipo stdClass con el segundo registro
    $registro = $resultados->fetch_object();
    //ller nombre
    echo $registro->nombre . "<br/>";
    var_dump($registro);

    // puedo con fetch_object() crear un objeto de tipo personas

    $registro = $resultados->fetch_object("clases\Personas");
    //leer nombre
    echo $registro->nombre . "<br/>";
    var_dump($registro);


    // quiero utilizar fetch_object() para crear un objeto
    // de tipo Personasconstructor
    // no npuedo porqu tiene constructor y me crearia un objeto vacio
    //para solucionar el problema he coocado un constructor especial

    //opcion 1
    $registro = $resultados->fetch_object("clases\Personasconstructor");
    var_dump($registro);

    //utilizar fetch_assoc() y new para crear un objeto de tipo Personasconstructor
    //opcion 2
    $registro = new Personasconstructor($resultados->fetch_assoc());
    var_dump($registro);
