<?php

namespace clases;

class Personas{
    public int $idPersona;
    public string $nombre;
    public string $apellidos;
    public int $edad;
    public string $fechaNacimiento;
    public string $poblacion;
    public string $codigoPostal;

    public function __toString(){
        $salida ="<ul>";
        foreach ($this as $campo => $valores) {
            # code...
            $salida .= "<li>{$campo}: {$valores}</li>";
        }
        $salida .= "</ul>";
        return $salida;
    }

}