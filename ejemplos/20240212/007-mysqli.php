<?php

use clases\Personas;
use clases\Personasconstructor;
require_once "autoload.php";
require_once "funciones.php";

//mostrar todos los datos de la tabla personas1
// de la base de datos personas 
// del servidor de base de datos que esta en el localhost


//quiero que utilicemos el metodo fecth_object
// utilizando una personas de tipo Personas
$host = "localhost";
$usuario = "root";
$contraseña = "";
$base_de_datos = "personas1";

// 1 paso
// realizar la conexion

$conexion = new mysqli($host,$usuario,$contraseña,$base_de_datos);
// 2 paso
// comprobar que la conxion se ha realizado correctamente
if($conexion->connect_error){
    die("la conexion ha fallado: " . $conexion->connect_error);
}
// 3 paso
// realizar la consulta
$consulta = "SELECT * FROM personas";
$resultados= $conexion->query($consulta);


// 4 paso
// colocar los resultados en pantalla
// ultilizamos un while

//$registro = $resultados->fetch_object();
// en registro tenemos un objeto de tipo stdclass
// donde tenemoslo datos dl registro



while($registro = $resultados->fetch_assoc()){
    $registrosObjeto =  new Personasconstructor($registro);
    $registro[] = $registrosObjeto;
}

echo gridViewTable($registro);

//  paso cerrar la conexion 
$conexion->close();