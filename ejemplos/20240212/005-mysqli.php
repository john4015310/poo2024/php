<?php
//cargo libreria con funciones 
require_once "funcionesObjetos.php";
//use clases\Personas;
// require_once "autoload.php";
// -- UTILIZAMOS FETCH_ALL --
//mostrar todos los datos de la tabla personas1
// de la base de datos personas 
// del servidor de base de datos que esta en el localhost


//quiero que utilicemos el metodo fecth_all
// utilizando un array asociativo

//denifinir la informacion de conexion a la base de datos
$host = "localhost";
$usuario = "root";
$contraseña = "";
$base_de_datos = "personas1";

// 1 paso
// realizar la conexion
$conexion = new mysqli($host,$usuario,$contraseña,$base_de_datos);

// 2 paso
// comprobar que la conexion se ha realizado correctamente
if($conexion->connect_error){
    die("la conexion ha fallado: " . $conexion->connect_error);
}
// 3 paso
// realizar la consulta
$consulta = "SELECT * FROM personas";
$resultados= $conexion->query($consulta);

// Llamo a un gridviewtable y le paso la consulta sin ejecutar
// como msqli_result
echo gridViewTable($resultados);