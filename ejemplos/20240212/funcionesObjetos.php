<?php

// voy a realizar un gridViewtable mas versatil
//este grid view utiliza fetch_all
/**
 * 
 * Funcion que retorna una tabla con los registros pasados y solo los campos
 * que queramos. Los registros se los tengo que pasar como un objeto de tipo
 * mysqli_result
 * @param mysqli_result $resultados $resultados un objeto de tipo mysqli_result
 * @param array $camposMostrar campos a mostrar
 * @return string
 */

// function gridViewTable(mysqli_result $resultados, array $camposMostrar = null):string{
    
//     // creo un aaray con los registros
//     $registros = $resultados->fetch_all(MYSQLI_ASSOC);
//     // comprobar si hay registros
//     if(count($registros)){
//         if (is_null($camposMostrar)) {
//                 // leo los campos a mostrar si no los paso como argumento
//                 // los campos los leo del primer registro
//                 $camposMostrar = array_keys($registros[0]);
//             }

//         $salida= "<table border = '1'>";
//         // añadir los nombres e los campos a mostrar
//         if(is_null($camposMostrar)){
//             $camposMostrar = array_keys($registros[0]);
//         }
//         //cabecera
//         $salida .= "<thead><tr>";
//         foreach($camposMostrar as $campo){
//             $salida .= "<td>" . $campo . "</td>";
//         }
//         $salida .= "</tr></thead>";

//         //dato registros

//             foreach ($registros as $registro) {
//                 //cadad fila
//                 $salida .= "<tr>";
//                 //campos 
//                 foreach($camposMostrar as $campo){
//                     $salida .= "<td>" . $registro[$campo] . "</td>";
//                 }
//                 $salida .= "</tr>";
                
//             }

//         $salida .= "</table>";
        
//     }else{
//         $salida = "<div>No hay registros a mostrar</div>";
//     }
//     return $salida;
// }


// este gridview voy a utilizar fetch_array

/* 
* Funcion que retorna una tabla con los registros pasados y solo los campos
* que queramos. Los registros se los tengo que pasar como un objeto de tipo
* mysqli_result
* @param mysqli_result $resultados $resultados un objeto de tipo mysqli_result
* @param array $camposMostrar campos a mostrar
* @return string
*/
// function gridViewTable(mysqli_result $resultados, array $camposMostrar = null):string{
//     //leo el primer registro en un array unidimensional asociativo
//     if($resultados->num_rows > 0){
//         $registro = $resultados->fetch_array(MYSQLI_ASSOC);
//         if(is_null($camposMostrar)){
//             //leo todos los campos
//                 $camposMostrar = array_keys($registro);
//         }
//         $salida = "<table border = '1'>";
//         // cabecera
//         $salida .= "<thead><tr>";
//         foreach($camposMostrar as $campo){
//             $salida .= "<td>" . $campo . "</td>";
//         }
//         $salida .= "</tr>";

//         //muestro el registro
//         $salida .= "<tr>";
//         foreach ($camposMostrar as $campo) {
//             $salida .= "<td>" . $registro[$campo] . "</td>";
//         }
//         $salida .= "</tr>";
//         // creo un aaray con los registros
//         while($registro = $resultados->fetch_array()){
//             $salida .= "<tr>";
//             //campos 
//             foreach($camposMostrar as $campo){
//                 $salida .= "<td>" . $registro[$campo] . "</td>";
//             }
//             $salida .= "</tr>";
//         }
//         $salida .= "</table>";
//     }else{
//         $salida = "<div>No hay registros a mostrar</div>";
//     }
//    return $salida;
// }



// voy a realizar un gridViewtable mas versatil
//este grid view utiliza fetch_all

// en este gridView voy a utilizar fetch_array
// sin crear un array con todo los registros

// function gridViewTable(mysqli_result $resultados, array $camposMostrar = null): string
// {
//     // leo el primer registro en un array unidimensional asociativo
//     $registro = $resultados->fetch_array(MYSQLI_ASSOC);

//     if (is_null($camposMostrar)) {
//         // leo todos los campos
//         $camposMostrar = array_keys($registro);
//     }
//     $salida = "<table border='1'>";

//     // cabecera
//     $salida .= "<thead><tr>";
//     foreach ($camposMostrar as $campo) {
//         $salida .= "<td>" . $campo . "</td>";
//     }
//     $salida .= "</tr></thead>";

//     // muestro el primer registro
//     $salida .= "<tr>";
//     foreach ($camposMostrar as $campo) {
//         $salida .= "<td>" . $registro[$campo] . "</td>";
//     }
//     $salida .= "</tr>";
//     // creo un array con los registros
//     while ($registro = $resultados->fetch_array()) {
//         $salida .= "<tr>";
//         foreach ($camposMostrar as $campo) {
//             $salida .= "<td>" . $registro[$campo] . "</td>";
//         }
//         $salida .= "</tr>";
//     }
//     $salida .= "</table>";
//     return $salida;
// }

//----------------------------------------------------------------------------------------------
// Voy a realizar el gridViewTable con fetch_object

function gridViewTable(mysqli_result $resultados, array $camposMostrar = null):string{
    //leo el primer registro en un array unidimensional asociativo
    if($resultados->num_rows > 0){
        $registro = $resultados->fetch_object();
        if(is_null($camposMostrar)){
            //leo todos los campos
                $camposMostrar = array_keys(get_object_vars($registro));
        }
        $salida = "<table border = '1'>";
        // cabecera
        $salida .= "<thead><tr>";
        foreach($camposMostrar as $campo){
            $salida .= "<td>" . $campo . "</td>";
        }
        $salida .= "</tr>";

        //muestro el registro
        $salida .= "<tr>";
        foreach ($camposMostrar as $campo) {
            $salida .= "<td>" . $registro->$campo . "</td>";
        }
        $salida .= "</tr>";
        // muestro el resto de registros
        while($registro = $resultados->fetch_object()){
            $salida .= "<tr>";
            //campos 
            foreach($camposMostrar as $campo){
                $salida .= "<td>" . $registro->$campo . "</td>";
            }
            $salida .= "</tr>";
        }
        $salida .= "</table>";
    }else{
        $salida = "<div>No hay registros a mostrar</div>";
    }
   return $salida;
}