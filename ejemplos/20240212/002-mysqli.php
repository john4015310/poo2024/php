<?php
require_once "funciones.php";
// -- UTILIZAMOS FETCH_ARRAY --
//mostrar todos los datos de la tabla personas1
// de la base de datos personas 
// del servidor de base de datos que esta en el localhost


//quiero que utilicemos el metodo fecth_all
// utilizando un array asociativo

//denifinir la informacion de conexion a la base de datos
$host = "localhost";
$usuario = "root";
$contraseña = "";
$base_de_datos = "personas1";

// 1 paso
// realizar la conexion
$conexion = new mysqli($host,$usuario,$contraseña,$base_de_datos);

// 2 paso
// comprobar que la conxion se ha realizado correctamente
if($conexion->connect_error){
    die("la conexion ha fallado: " . $conexion->connect_error);
}
// 3 paso
// realizar la consulta
$consulta = "SELECT * FROM personas";
$resultados= $conexion->query($consulta);


// 4 paso
// colocar los resultados en pantalla
// utilizar el metodo fetch_array para mostrar los datos
// ultilizamos un while para mostrar dato a dato
while($registro = $resultados->fetch_array(MYSQLI_ASSOC)){
    // registro es un array unidimensional de tipo asociativo con los datos de un registro
    // echo detailView($registro);
    //puedo almacenar cada registro en un array y despues lo muestro on gridViewTable
    $registros[]=$registro;
}

echo gridViewTable($registros);
//  paso cerrar la conexion 
$conexion->close();