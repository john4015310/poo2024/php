<?php
/**
 * 
 * Funcion que retorna una tabla con los registros pasados 
 * @param array $registros registros a mostrar
 * @param array $camposMostrar campos a mostrar
 * @return string tabla
 */


// function gridViewTable(array $registros, array $camposMostrar = null):string{

//     $salida= "<table border = '1'>";
//     // añadir los nombres e los campos a mostrar
//     if(is_null($camposMostrar)){
//         $camposMostrar = array_keys($registros[0]);
//     }
//     //cabecera
//     $salida .= "<thead><tr>";
//     foreach($camposMostrar as $campo){
//         $salida .= "<td>" . $campo . "</td>";
//     }
//     $salida .= "</tr></thead>";



//     for($indice = 0; $indice < count($registros); $indice++){
        
//         //cuerpo
//         $salida .= "<tr>";
//         foreach ($camposMostrar as $campo) {
//             # code...
//             $salida .= "<td>" . $registros[$indice][$campo] . "</td>";
//         }
//         $salida .= "</tr>";
        

//     }
//     $salida .= "</table>";
//     return $salida;
// }



function gridViewTable(array $registros, array $camposMostrar = null):string{
    // comprobar si hay registros

    if(count($registros) > 0){
        if (is_null($camposMostrar)) {
                // leo los campos a mostrar si no los paso como argumento
                // los campos los leo del primer registro

                //quieero comprobar si el registro es un objeto
                if(is_object($registros[0])){
                    $camposMostrar = array_keys(get_object_vars($registros[0]));
                }else{
                    //si el primer registro es un array
                    $camposMostrar = array_keys($registros[0]);
                }
                // si el primer registro es un objeto
                // $camposMostrar = array_keys(get_object_vars($registros[0]));
            }

        $salida= "<table border = '1'>";
        // añadir los nombres e los campos a mostrar
        if(is_null($camposMostrar)){
            $camposMostrar = array_keys($registros[0]);
        }
        //cabecera
        $salida .= "<thead><tr>";
        foreach($camposMostrar as $campo){
            $salida .= "<td>" . $campo . "</td>";
        }
        $salida .= "</tr></thead>";

        //dato registros

            foreach ($registros as $registro) {
                //cadad fila
                $salida .= "<tr>";
                //campos 
                foreach($camposMostrar as $campo){
                    if(is_object($registro)){
                        //si el registro es un objeto
                        $salida .= "<td>" . $registro->$campo . "</td>";
                    }
                    else{
                        //si el registro es un array
                        $salida .= "<td>" . $registro[$campo] . "</td>";
                    }
                }
                $salida .= "</tr>";
                
            }

        $salida .= "</table>";
        
    }else{
        $salida = "<div>No hay registros a mostrar</div>";
    }
    return $salida;
}

function gridViewTable1(array $registros, array $camposMostrar = null): string
{

    // comprobar si hay registros
    if (count($registros) > 0) {
        if (is_null($camposMostrar)) {
            // leo los campos a mostrar si no los paso como argumento
            // los campos los leo del primer registro
            $camposMostrar = array_keys($registros[0]);
        }
        $salida = "<table border='1'>";
        // añadir los nombres de los campos a mostrar

        // cabecera
        $salida .= "<thead><tr>";
        foreach ($camposMostrar as $campo) {
            $salida .= "<td>" . $campo . "</td>";
        }
        $salida .= "</tr></thead>";

        // datos, registros
        foreach ($registros as $registro) {
            // cada fila
            $salida .= "<tr>";
            // campos
            foreach ($camposMostrar as $campo) {
                $salida .= "<td>" . $registro[$campo] . "</td>";
            }
            $salida .= "</tr>";
        }

        $salida .= "</table>";
    } else {
        $salida = "<div>No hay registros a mostrar</div>";
    }
    return $salida;
}


function gridViewTable2(array $registros, array $camposMostrar = null): string
{

    // comprobar si hay registros
    if (count($registros) > 0) {
        if (is_null($camposMostrar)) {
            // leo los campos a mostrar si no los paso como argumento
            // los campos los leo del primer registro
            $camposMostrar = array_keys(get_object_vars($registros[0]));
        }
        $salida = "<table border='1'>";
        // añadir los nombres de los campos a mostrar

        // cabecera
        $salida .= "<thead><tr>";
        foreach ($camposMostrar as $campo) {
            $salida .= "<td>" . $campo . "</td>";
        }
        $salida .= "</tr></thead>";

        // datos, registros
        foreach ($registros as $registro) {
            // cada fila
            $salida .= "<tr>";
            // campos
            foreach ($camposMostrar as $campo) {
                $salida .= "<td>" . $registro->$campo . "</td>";
            }
            $salida .= "</tr>";
        }

        $salida .= "</table>";
    } else {
        $salida = "<div>No hay registros a mostrar</div>";
    }
    return $salida;
}



/**
 * 
 * Funcion que retorna una lista con los registros pasados y solo los campos
 * que queramos
 * @param array $registros registros a mostrar como arrays
 * @param array $campos
 * @return string lista
 */
function ListView(array $registros, array $camposMostrar = null): string
{
    // comprobar si hay registros
    if(count($registros)>0){
        if (is_null($camposMostrar)) {
            $camposMostrar = array_keys($registros[0]);
        }
        $salida = "";
        foreach ($registros as $registro) {
            $salida .= "<ul>";
            foreach ($camposMostrar as $campo) {
                $salida .= "<li>{$campo}: " . $registro[$campo] . "</li>";
            }
            $salida .= "</ul>";
        }
    }else{
        $salida = "<div>No hay registros a mostrar</div>";
    }
    return $salida;
}

function detailView(array $registro, array $camposMostrar = null): string
{
    if (is_null($camposMostrar)) {
        $camposMostrar = array_keys($registro);
    }
    $salida = "<ul>";
    foreach ($camposMostrar as $campo) {
        $salida .= "<li>{$campo}: " . $registro[$campo] . "</li>";
    }
    $salida .= "</ul>";
    return $salida;

}

