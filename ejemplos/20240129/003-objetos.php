<?php
use clases\herencia\Vaca;
use clases\herencia\Perro;

require 'autoload.php';

$vaca1 = new Vaca("ernesta");

var_dump($vaca1);

$perro1 = new Perro();
var_dump($perro1);

$perro2 = new Perro("fifi");
var_dump($perro2);
