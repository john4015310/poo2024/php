<?php
use clases\animales\Animal;
use clases\animales\Perro;
use clases\animales\Persona;
use clases\animales\Vaca;

require_once 'autoload.php';


$vaca1 = new Vaca("Vaca", 2, "blanca","no");

$persona1 = new Persona("John", "Calle Vargas", "hombre");
$perro1 = new Perro("Thor", 5, "gris",true,$persona1);
$animal1 = new Animal("Niina",10, "blanco");


var_dump($perro1);
var_dump($vaca1);
var_dump($persona1);
var_dump($animal1);
