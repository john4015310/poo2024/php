<?php
namespace clases\herencia;
require 'autoload.php';

class Perro extends Animal{
    
    public bool $vacunado;
    public ?Persona $persona;

    public function __construct(string $nombre="", float $peso=0, string $color="", bool $vacunado=false, ?Persona $persona = null){
        parent::__construct($nombre, $peso, $color);
        $this->vacunado = $vacunado;
        $this->persona = $persona;
    }
}