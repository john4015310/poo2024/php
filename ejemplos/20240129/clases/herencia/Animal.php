<?php
namespace clases\herencia;
include "autoload.php";
class Animal{
    public string $nombre;
    public float $peso;
    public string $color;
    public function __construct(string $nombre="animal", float $peso=0, string $color="sin color"){
        $this->nombre = strtoupper($nombre);
        $this->peso = $peso;
        $this->color = $color;
    }
}