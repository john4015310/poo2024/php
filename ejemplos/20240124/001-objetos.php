<?php
spl_autoload_register(function ($class) {
    require "clases/". $class . '.php';
});

//quiero introducir lo siguiente
//personas
//jorge
//pedro
//juan

//oficios
//programador, 10, 50
//ingeniero, 20, 50

//donde trabaja cada uno 
//jorge programador
//pedro ingeniero
//juan programador
$persona1=new Humano('jorge');
$persona2=new Humano('pedro');
$persona3=new Humano('juan');

$oficio1=new Oficio('programador',10,50);
$oficio2=new Oficio('ingeniero',20,50);

$trabajan1=new Trabajan($persona1, $oficio1);
$trabajan2=new Trabajan($persona2, $oficio2);
$trabajan3=new Trabajan($persona3, $oficio1);

echo $trabajan1->persona->presentarse();
echo "<br>";
echo $trabajan1->oficio->calcular();

var_dump($trabajan1, $trabajan2, $trabajan3);
