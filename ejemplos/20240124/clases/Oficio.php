<?php
class Oficio
{
    //atributos
    public string $nombre;
    public float $salirioBase;
    public int $horasSemnales;

    //constructor
    public function __construct(string $nombre="", float $salirioBase=0, int $horasSemnales=0){

        $this->nombre = $nombre;
        $this->salirioBase = $salirioBase;
        $this->horasSemnales = $horasSemnales;
    }
    //metodos
    public function calcular(): float{

        return $this->salirioBase * $this->horasSemnales;
    }

    
}