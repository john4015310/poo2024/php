<?php
class Humano
{
    //atributos
    public string $nombre;
    public string $sexo;
    public string $fechaNacimiento;
    //constructores
    public function __construct(string $nombre="", string $sexo="", string $fechaNacimiento=""){
        $this->nombre = $nombre;
        $this->sexo = $sexo;
        $this->fechaNacimiento = $fechaNacimiento;
    }
    //metodos

    public function presentarse(): string{
        return "Hola soy ".$this->nombre." y mi fecha de nacimiento es ".$this->fechaNacimiento;
    }

    
}