<?php
    //crear dos variables numeros enteros
    $num1 = 10;
    $num2 = 20;
    $salida="";

    //quiero una variables salida que me indique 
    //si num1 es mayor que num2
    //si num2 es mayor que num1
    //o si son iguales

    if($num1 > $num2){
        $salida="El numero1= {$num1} es mayor que el numero2 = {$num2}";
    }else if($num2 > $num1){
        $salida="El numero2= {$num2} es mayor que el numero1 = {$num1}";
    }else{
        $salida="Los dos numeros son iguales";
    }
    //crear un funcion que reciba dos numero y que devuelva la media
    //de los dos
   
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<div id="caja">
    <form action="003seleccionFormulario.php">
        <div>
            <label for="numero1">Numero1</label>
            <input type="number" id="numero1" name="numero1" placeholder="Introduce un numero">
        </div>

        <div>
            <label for="numero2">Numero2</label>
            <input type="number" id="numero2" name="numero2" placeholder="Introduce un numero">
        </div>

        <div>
            <button type="submit">Enviar</button>
            <button type="reset">Limpiar</button>
            
        </div>
        
        
    </form>
    </div>
</body>
</html>