<?php
namespace clases;
class Dimension
{
    private float $alto;
    private float $ancho;
    private float $profundo;

    public function __construct(){
        $this->alto = 0;
        $this->ancho = 0;
        $this->profundo = 0;
    }

    public function setAlto(float $alto):self{
        $this->alto = $alto;
        return $this;
    }

    public function setAncho(float $ancho):self{
        $this->ancho = $ancho;
        return $this;
        
    }

    public function setProfundo(float $profundo):self{
        $this->profundo = $profundo;
        return $this;
    }
    
    public function getAlto():float{
        return $this->alto;
    }

    public function getAncho():float{
        return $this->ancho;
    }

    public function getProfundo():float{
        return $this->profundo;
    }

    public function getVolumen():float{
        return $this->alto * $this->ancho * $this->profundo;
    }

    public function __toString():string{
        $salida= "Alto: ".$this->alto.", Ancho: ".$this->ancho.", Profundidad: ".$this->profundo.", Volumen: ".$this->getVolumen();
        return $salida;
    }

}