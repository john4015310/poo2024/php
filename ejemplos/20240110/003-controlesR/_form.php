<form method="post">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" placeholder="Introduce nombre" title="Introduce tu nombre" required>
    </div>
    <div>
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" placeholder="Introduce apellidos" title="Introduce tus apellidos" required>
    </div>
    <div>
        <button type="submit">Enviar</button>
        <button type="reset">Limpiar</button>
    </div>
</form>