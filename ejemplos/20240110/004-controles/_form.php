<form method="post">
        <fieldset >
            <legend>Formulario Suma</legend>
              <div class="mb-3">
                  <label for="numero1" class="form-label">Número 1 </label>
                  <input type="number" class="form-control" id="numero1" name="numero1" placeholder="Introducir un número" required>
              </div>
              <div class="mb-3">
                  <label for="numero2" class="form-label">Número 2 </label>
                  <input type="number" class="form-control" id="numero2" name="numero2" placeholder="Introducir un número" required>
              </div>
              <div class="mb-3">
                  <label for="numeros" class="form-label">Números</label>
                  <input type="text" class="form-control" id="numeros" name="numeros" placeholder="Introducir números separados por ; " required>
              </div>
              
              <button type="submit" class="btn btn-primary mb-3">Submit</button>
              <button type="reset" class="btn btn-primary mb-3">Reset</button>


        </fieldset>
    </form>