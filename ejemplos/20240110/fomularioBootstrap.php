<?php
  $num1=0;
  $num2=0;
  $numeros="";
  $arrayNum=[];
  $suma=null;
  if($_POST){
    $num1=$_POST["numero1"]?? 0;
    $num2=$_POST["numero2"]?? 0;
    $numeros=$_POST["numero3"];
    $suma=$num1+$num2;
    $arrayNum= explode(";", $numeros);

    foreach ($arrayNum as $key) {
      # code...
      $suma+=$key;
    }
    // $suma+=$arrayNum[0];
    
    

  }

  
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    
    <div class="container">
        <!-- Content here -->
    <form method="post">
        <fieldset >
            <legend>Formulario Suma</legend>
              <div class="mb-3">
                  <label for="numero1" class="form-label">Número 1 </label>
                  <input type="number" class="form-control" id="numero1" name="numero1" placeholder="Introducir un número" required>
              </div>
              <div class="mb-3">
                  <label for="numero2" class="form-label">Número 2 </label>
                  <input type="number" class="form-control" id="numero2" name="numero2" placeholder="Introducir un número" required>
              </div>
              <div class="mb-3">
                  <label for="numero3" class="form-label">Números</label>
                  <input type="text" class="form-control" id="numero3" name="numero3" placeholder="Introducir números separados por ; " required>
              </div>
              <div class="row mb-3">
                  <div class="col">
                    <div class="card">
                        <div class="card-header">
                          Suma
                        </div>
                        <div class="card-body">
                          <p class="card-text"><?=$suma?></p>
                          
                        </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card">
                        <div class="card-header">
                          Números
                        </div>
                        <div class="card-body">
                          <?php
                            if($_POST){
                              echo "<p class='card-text'>{$num1}</p>";
                              echo "<p class='card-text'>{$num2}</p>";
                            foreach ($arrayNum as $key) {
                            ?>
                            <p class="card-text"><?=$key?></p>
                            <?php
                            }
                          }
                          ?>
                          
                          

                          
                        </div>
                    </div>
                  </div>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-primary">Reset</button>
        </fieldset>
    </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>