<?php
$resultado="";
if($_POST){
    $resultado= isset($_POST["frutas"]) ? implode(", ", $_POST["frutas"]) : "No ha seleccionado fruta";
    
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Usar cuadros de lista</h1>
    <form method="post">
        <div>
        <label for="frutas">Elija sus frutas preferidas</label><br>
        <select name="frutas[]" id="frutas" multiple>

            <option value="manzana">Manzana</option>
            <option value="naranja">Naranja</option>
            <option value="pera">Pera</option>
            <option value="pomelo">Pomelo</option>
        </select>
        </div>
        <br>
        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    <?=$resultado?>
</body>
</html>