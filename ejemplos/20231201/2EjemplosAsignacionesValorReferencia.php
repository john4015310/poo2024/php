<?php

$numero1 = 0;

$numero2 = $numero1; //asigno el valor dl 1 al 2

$numero3 = &$numero1; //asigno una refencia de uno a tres

$numero1 = 45;

// cuanto valen las variables
// numero1
// numero2
// numero3
echo "numero1 vale " . $numero1 . "<br>";
echo "numero2 vale " . $numero2 . "<br>";
echo "numero3 vale " . $numero3 . "<br>";

// realizar la misma impresion sin utilizar concatenar
// directamente con las comillas

echo "numero1 vale {$numero1}<br>";
echo "numero2 vale {$numero2}<br>";
echo "numero3 vale {$numero3}<br>";

// realizar la misma impresion pero con el modo contraido de impresion
?>

numero1 vale <?=$numero1?><br>
numero2 vale <?=$numero2?><br>
numero3 vale <?=$numero3?><br>
