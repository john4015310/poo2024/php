<?php
header("Content-type: image/png");
// crear una imagen vacia
$image = imagecreate(400, 300);

// rellenarla con el color de fondo
$bg = imagecolorallocate($image, 0, 0, 0);

// crear el color para el poligono
$col_poly = imagecolorallocate($image, 255, 255, 255);

// dibujar el poligono
imagepolygon($image, 
             array (
                    0, 0,
                    100, 200,
                    300, 200
             ),
             3,
             $col_poly);

// mostrar la imagen

imagepng($image);

?>