﻿-- Crear la base de datos "personas"
DROP DATABASE IF EXISTS tablonAnuncio;

CREATE DATABASE tablonAnuncio;

-- Usar la base de datos "personas"
USE tablonAnuncio;

-- Crear la tabla "personas"
CREATE TABLE mensajes (
    id INT AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    mensaje VARCHAR(500) ,
    fechaHora datetime,
    PRIMARY KEY (id)

);