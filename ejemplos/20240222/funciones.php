<?php
    function controlErrores(){
        //desactivar errores en pantalla
        //error_reporting(0);
        // convertir los errroes sde la libreria mysqli en warnings
        mysqli_report(MYSQLI_REPORT_OFF);
    }

    //funcion que crea la tabla
    function gridViewBotones(mysqli_result $resultados){
        if ($resultados->num_rows >0){
    
            $registros= $resultados->fetch_all(MYSQLI_ASSOC);
            $salida = "<table border = 1>";
            $salida .= "<thead><tr>";

            $campos = array_keys($registros[0]);
            foreach ($campos as $campo) {
                # code...
                $salida .= "<th>$campo</th>";
            }
            //añado columna de botones
            $salida .= "<th>Acciones</th>";
            $salida .= "<tr></thead>";
            foreach($registros as $registro){
                $salida .= "<tr>";
                foreach($registro as $clave => $valor){
                    $salida .= "<td>$valor</td>";
                }
                $salida .= "<a href='002-eliminar.php?id=" . $registro['id'] ."'>Eliminar</a>";
                $salida .= "</tr>";
            }
            $salida .= "</table>";
            }else{
                $salida="No hay registros";
            }
            return $salida;
    }


