<?php
    session_start();
    $conectado = false;
    $_SESSION["usuarios"] = [];
    $parametros = require_once("parametros.php");
    require_once("funciones.php");
    //Controlar los errores en la ejecucion
    //controlErrores();

    //Conectar a la base de datos
    $conexion = @new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);

    //Comprobar la conexion
    if ($conexion->connect_error) {
        die("La llamada ha fallado: " . $conexion->connect_error);
    }
    //preparo la consulta
    $sql= "SELECT * FROM mensajes"; 
    
    //creo un objeto de tipo mysqli_result e la consulta
    if($resultados=$conexion->query($sql)){
        //crear una tabla con los registros
        $salida= gridViewBotones($resultados);
       
    }else{
        $salida="Error al ejecutar la consulta: "  . $conexion->error;
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <h1><?=$parametros["aplicacion"]["nombreAplicacion"]?></h1>
        <?php require_once("_nav.php"); ?>
        <?php

            if ($conectado == false) {
                require_once("_login.php");
                $conectado = true;
                $_SESSION["usuario"] = $_POST["usuario"] ?? null;
            }
            
            var_dump($_SESSION);
        ?>
    </body>
    </html>