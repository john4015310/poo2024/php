<?php

$parametros = require_once("parametros.php");
require_once("funsiones.php");

//desactivar errores
controlErrores();

//conexion a base de datos

$conexion = @new mysqli($parametros["servidor"], $parametros["usuario"], $parametros["password"], $parametros["nombreBd"]);

//compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("La conexión ha fallado: " . $conexion->connect_error);
}


$sql= "INSERT INTO empleados 
(nombre, apellidos, edad, poblacion, codigoPostal, fechaNacimiento)
VALUES ('John', 'Wick', 30, 'New York', '0007', '1990-01-01')";


if($conexion->query($sql)){
    $salida= "Registro insertado correctamente";
}else{
    $salida= "Error: " . $sql . "<br>" . $conexion->error;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$parametros["nombreAplicacion"]?></title>
</head>
<body>
    <h1><?=$parametros["nombreAplicacion"]?></h1>
    <div>
        <?=$salida?>
    </div>
</body>
</html>