<?php

$parametros = require_once("parametros.php");
require_once("funsiones.php");

//heinicializado la variable que muestra
//el mensaje de si ha sido correcta
//la insercion
$salida = "";
$accion = "Insertar";
//he inicializado el array de datos
//que me muestra esto em el formulario
$datos = [
    "nombre" => "",
    "apellidos" => "",
    "edad" => "",
    "poblacion" => "Santander",
    "codigoPostal" => "",
    "fechaNacimiento" => ""
];
//desactivar errores
controlErrores();

//conexion a base de datos
//comprobar si he pulsado el boton de insertar
if($_POST){
    $datos["nombre"] = $_POST["nombre"];
    $datos["apellidos"] = $_POST["apellidos"];
    $datos["edad"] = $_POST["edad"];
    $datos["poblacion"] = $_POST["poblacion"];
    $datos["codigoPostal"] = $_POST["codigoPostal"];
    $datos["fechaNacimiento"] = $_POST["fechaNacimiento"];

//conexión de la base de datos
$conexion = @new mysqli($parametros["servidor"], $parametros["usuario"], $parametros["password"], $parametros["nombreBd"]);

//compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("La conexión ha fallado: " . $conexion->connect_error);
}


$sql= "INSERT INTO empleados 
(nombre, apellidos, edad, poblacion, codigoPostal, fechaNacimiento) VALUES 
(
    '{$datos["nombre"]}',
    '{$datos["apellidos"]}',
     {$datos["edad"]},
    '{$datos["poblacion"]}',
    '{$datos["codigoPostal"]}',
    '{$datos["fechaNacimiento"]}'
)";


if($conexion->query($sql)){
    $salida= "Registro insertado correctamente";
    $salida= "<br><br><a href='008-listarRegistrosUpdateDelete.php'>Ver todos los registros</a>";
}else{
    $salida= "Error: " . $sql . "<br>" . $conexion->error;
}

$conexion->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$parametros["nombreAplicacion"]?></title>
</head>
<body>
    <h1><?=$parametros["nombreAplicacion"]?></h1>
    <div>
        <?php
        require_once("_form.php");
        ?>
    </div>
    <div>
        <?=$salida?>
    </div>
</body>
</html>