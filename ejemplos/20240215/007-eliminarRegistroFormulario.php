<?php

$parametros = require_once("parametros.php");
require_once("funsiones.php");

//desactivar errores
controlErrores();

//conexion a base de datos

$conexion = @new mysqli($parametros["servidor"], $parametros["usuario"], $parametros["password"], $parametros["nombreBd"]);

//compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("La conexión ha fallado: " . $conexion->connect_error);
}

//el registro a borrar me llega por get
$sql= "DELETE  FROM empleados WHERE id =". $_GET["id"];


if($conexion->query($sql)){
    $salida="Registro eliminado correctamente";
    $salida="<a href='003-listarRegistros.php'>Listar Registros</a>";

}else{
    $salida="Error al eliminar el registro: "  . $conexion->error;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$parametros["nombreAplicacion"]?></title>
</head>
<body>
    <h1><?=$parametros["nombreAplicacion"]?></h1>
    <div>
        <?=$salida?>
    </div>
</body>
</html>