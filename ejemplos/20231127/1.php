<?php
//crear una variable de tipo numerica

$a=1;
$b=2.3;

//crear varibles de tipo string

$c="ejemplo";
$d="1";

//crear variable de tipo boolean

$e= true;
$f= false;

//crear variable tipo array

$g=[1,2,"hola"];
$h=array(23,3);

//crear variable de tipo null

$i= null;

//para comprobar valor y tipo
//utilizamos var_dump

var_dump($a,$d,$e,$h);
// para comporbar tipo 
//gettype

echo gettype($d);

// para comprobar el valor
// echo
//utilizar una impresion corta

echo "<div>d={$d}</div>";


?>

<div>d=<?=$d?></div>



