<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
<?php
// datos del alumno con la nota mas alta

$datos=[
    "id"=> 12,
    "nombre"=> "Eva",
    "apellidos"=> "Gomez Palomo",
    "poblacion"=>"Laredo"
];

?>
<table>
    <tr class="t1">
        <td>Campo</td>
        <td>Valor</td>
    </tr>
    <tr>
        <td>ID</td>
        <td>
            <?=$datos["id"]?>
        </td>
    </tr>
    <tr>
        <td>Nombre</td>
        <td>
            <?=$datos["nombre"]?>
        </td>
    </tr>
    <tr>
        <td>Apellidos</td>
        <td>
            <?=$datos["apellidos"]?>
        </td>
    </tr>
    <tr>
        <td>Poblacion</td>
        <td>
            <?=$datos["poblacion"]?>
        </td>
    </tr>
</table>