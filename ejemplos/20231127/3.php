<?php
//variables a utilizar
$a=10;
$b=3;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Ejemplo 3</title>
</head>
<body>
    <!--Quiero que coloquemos los resultados de las siguientes operaciones
            a+b
            a-b
            a*b
            a resto b
            a elevado b
            a dividido entre b
            a es iguala b
            a menor o igual que b 
        
            -->
    <div>
    <table >
        <tr class="t1">
            <td>
                Operacion
            </td>
            <td>
                Resultado
            </td>
        </tr>

        <tr>
            <td>Suma</td>
            <td>
                <?=$a+$b?>
            </td>
        </tr>

        <tr>
            <td>Resta</td>
            <td>
            <?=$a-$b?>
            </td>
        </tr>

        <tr>
            <td>Producto</td>
            <td>
                <?=$a*$b?>
            </td>
        </tr>

        <tr>
            <td>Resto</td>
            <td>
                <?=$a%$b?>
            </td>
        </tr>
        
        <tr>
            <td>Potencia</td>
            <td>
                <?=$a**$b?>
            </td>
        </tr>

        <tr>
            <td>Division</td>
            <td>
                <?=$a/$b?>
            </td>
        </tr>

        <tr>
            <td>A es igual B</td>
            <td>
                <?php 
                if($a==$b){
                    echo "Si, son iguales";
                }else{
                    echo "No, son iguales";
                }
                ?>
            </td>
        </tr>

        <tr>
            <td>A es Menor o igual que B</td>
            <td>
            <?php 
                if($a<=$b){
                    echo "A es Menor o igual que B";
                }else{
                    echo "A no es Menor o igual que B";
                }
                ?>
            </td>
        </tr>
    </table>
    </div>
</body>
</html>