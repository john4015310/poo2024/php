<?php
//Variables
$a=10;
$b=3;
//Procesos
$sumar=$a+$b;
$resta= $a-$b;
$producto= $a*$b;
$resto= $a%$b;
$potencia= $a**$b;
$division= $a/$b;
$igual="";
$menor="";

if($a==$b){
    $igual= "Si, son iguales";
}else{
    $igual= "No, son iguales";
}

if($a<=$b){
    $menor= "A es Menor o igual que B";
}else{
    $menor= "A no es Menor o igual que B";
}
//Array
$resultados=[
    "suma"=> $sumar,
    "resta"=> $a-$b,
    "producto"=> $a*$b,
    "resto"=> $a%$b,
    "potencia"=> $a**$b,
    "division"=> $a/$b,
    "esIgual"=> $igual,
    "esMenorI"=> $menor,
];
//$resultados["suma"]= $a+$b; estamos haciendo un push en cambio de la otra manera lo inicializamos directamente.


var_dump($resultados);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    
<table >
        <tr class="t1">
            <td>
                Operacion
            </td>
            <td>
                Resultado
            </td>
        </tr>

        <tr>
            <td>Suma</td>
            <td>
                <?=$sumar?>
            </td>
        </tr>

        <tr>
            <td>Resta</td>
            <td>
            <?=$resta?>
            </td>
        </tr>

        <tr>
            <td>Producto</td>
            <td>
                <?=$producto?>
            </td>
        </tr>

        <tr>
            <td>Resto</td>
            <td>
                <?=$resto?>
            </td>
        </tr>
        
        <tr>
            <td>Potencia</td>
            <td>
                <?=$potencia?>
            </td>
        </tr>

        <tr>
            <td>Division</td>
            <td>
                <?=$division?>
            </td>
        </tr>

        <tr>
            <td>A es igual B</td>
            <td>
            <?=$igual?>
            </td>
        </tr>

        <tr>
            <td>A es Menor o igual que B</td>
            <td>
            <?=$menor?>
            </td>
        </tr>
    </table>
    </div>
</body>
</html>