<?php

    //una funcion que muestra alumnos
    //dato de un array bidimensional
    // donde en la primera dimension tengo los
    //registros y en la segunda dimension los campos
    //mostramos en una tabla

    function mostrarDatos($alumnos):string{
        $html="<table>";
        $c=0;
        foreach($alumnos as $alumno){
            
            $html.="<tr>";
            
            foreach($alumno as  $dato){
                
                $html.="<td>$dato</td>";
            }
            $html.="</tr>";
        }

        $html.="</table>";
        return $html;
    }


    //array walk

    function mostrarDatos2(array $datos):string{
        $html="<table>";
        foreach ($datos as $registro) {
            # code...
            $html.= "<tr>";
            array_walk($alumno, function($valor)use (&$html){
                $html.= "<td>$valor</td>";
                
            });
            $html.= "</tr>";

        }
        return $html;
    }




    function mostrarDatos3(array $datos):string
    {
        $html="<table>";
        array_walk($datos, function($alumno)use (&$html){
            $html.= "<tr>";
            array_walk($alumno, function($valor)use (&$html){
                $html.= "<td>$valor</td>";
                
            });
            $html.= "</tr>";
        });
        $html.= "</table>";
        return $html;
    }
    ?>

    <table>
        <tr>
            <td>id</td>
            <td>nombre</td>
            <td>apellido</td>
            <td>edad</td>
        </tr>
        <tr>
            <td>id</td>
            <td>nombre</td>
            <td>apellido</td>
            <td>edad</td>
        </tr>
    </table>

    <?php

        function mostrarCabeceras(array $datos):string{
            $html="<table><thead><tr>";
            foreach ($datos[0] as $registro => $alumno) {
                # code...
                $html.= "<th>$registro</th>";
            }
            $html .= "</tr></thead><tbody>";

            foreach ($datos as $alumno) {
                # code...
                $html .= "<tr>";
                foreach ($alumno as $dato) {
                    # code...
                    $html .= "<td>$dato</td>";
                }
                $html .= "</tr>";
            }
            $html .= "</tbody></table>";
            
            return $html;
        }

        /**
         * Otra version del mostrar cabeceras 3
         * 
         */

         function mostrarCabeceras1(array $datos):string{
            $html="<table>";
            $cabecera="<thead><tr>";
            $indices=array_keys($datos[0]);

            array_walk($indices,function($valores)use(&$cabecera){
                $cabecera.="<th>$valores</th>";
            });
            $cabecera.="</tr></thead>";
            
            foreach ($datos as $alumno) {
                # code...
                $html .= "<tr>";
                foreach ($alumno as $dato) {
                    # code...
                    $html .= "<td>$dato</td>";
                }
                $html .= "</tr>";
            }
            $html .= "</tbody></table>";
            
            return $html;
        }
    ?>