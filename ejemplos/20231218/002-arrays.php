<?php

//  funcion llamada mostrarArray que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array for
// solo admite arrays unidimensionales de tipo enumerado y secuenciales

function mostrarArray(array $datos): string
{
    $html = "<table>";
     
    for($c=0;$c<count($datos);$c++){
        $html .= "<tr>";
        $html .= "<th>{$c}</th>";
        $html .= "<td>{$datos[$c]}</td>";
        $html .= "</tr>";

    }
    // foreach ($datos as $key => $value) {
    //     $html .= "<tr>";
    //     $html .= "<th>$key</th>";
    //     $html .= "<td>$value</td>";
    //     $html .= "</tr>";
    // }
    $html .= "</table>";

    return $html;
}

function mostrarArray1(array $datos): string
{
    $html = "<table>";
    //en valores meto los valores del array oasado con argumnentos
    $valore= array_values($datos);
    // en indices meto los incides del array pasado como argumento
    $indices = array_keys($datos);
    //recorro el array con un bucle for
    for($c=0;$c<count($datos);$c++){
        $html .= "<tr>";
        $html .= "<th>{$indices[$c]}</th>";
        $html .= "<td>{$valore[$c]}</td>";
        $html .= "</tr>"; 
    }
    $html .= "</table>";
    return $html;
}


function mostrarArray2(array $datos): string
{
    $html = "<table>";

    foreach ($datos as $key => $value) {
        $html .= "<tr>";
        $html .= "<th>$key</th>";
        $html .= "<td>$value</td>";
        $html .= "</tr>";
    }
    $html .= "</table>";

    return $html;
}

/*============================================================================*/
//Array Walk
/*===========================================================================*/ 

function mostrarArray3(array $datos): string
{
        $html = "<table>";
        array_walk($datos, function($valor,$indice)use(&$html){
            $html .= "<tr>";
            $html .= "<th>{$indice}</th>";
            $html .= "<td>{$valor}</td>";
            $html .= "</tr>";
        });
        $html .= "</table>";

        return $html;
}

//  funcion llamada mostrarArray que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array con array_map

function mostrarArray4(array $datos): string
{
    $html = "<table>";
    $indices = array_keys($datos);
    $valores = array_values($datos);
    $datos1 = array_map(function ($valor,$indice){ //agrega o a cada elemente le agregas o modificas
        return "<tr><th>{$indice}</th><td>{$valor}</td></tr>";
    },$valores,$indices);

    $html .= implode("", $datos1);//crear un array en un string
    $html .= "</table>";
    return $html;
}

//  funcion llamada mostrarArray que recorre un array pasado como 
// primer parametro y retorna una tabla HTML

?>

<table>
    <tr>
        <th>RED</th>
        <td>Rojo</td>
    </tr>
    <tr>
        <th>Blue</th>
        <td>Azul</td>
    </tr>
</table>
