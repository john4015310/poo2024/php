<?php
require_once "002-arrays.php";

require "datos.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <h1>Array con For</h1>
    <?=mostrarArray($ciudades)?>
    <?=mostrarArray($edades)?>
    <?=mostrarArray($valores)?>

    <h1>Array con For</h1>
    <?=mostrarArray1($ciudades)?>

    <h1>Array con Foreah</h1>
    <?=mostrarArray2($ciudades)?>

    <h1>Array con arrayWalk</h1>
    <?=mostrarArray3($ciudades)?>

    <h1>Array con arrayMap</h1>
    <?=mostrarArray4($ciudades)?>

    <h1>mostrando datos</h1>
    <?=mostrarArray4($ciudades)?>
</body>

</html>
