<?php
//si el numero de unidades pedidas esta entre
// 0-10 : descuento 0
// 11-20 : descuento 10
// 21-30 : descuento 20
// mayor o igual a 31= descuento 30
$numero = 125;
$descuento= 0;
$salida= "";

//realizar el ejercicio utilizando if

if($numero <= 10 ){
    $salida= "No tiene descuento";
}else if ($numero >= 11 && $numero <= 20) {
    $descuento = 10;
    $salida = "Tiene descuento de {$descuento}";
}else if ($numero >= 21 && $numero <= 30){
    $descuento = 20;
    $salida = "Tiene descuento de {$descuento}";
}else{
    $descuento = 30;
    $salida = "No entra en el rango de descuento";
}

echo $salida;