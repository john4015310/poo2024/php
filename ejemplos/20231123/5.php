<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <?php
        // mostrar las siguientes variables en una lista
        // delcaracion de variables
        $a=10;
        $b="hola";
        $c=12.4;
        $d=true;
    ?>
    <div class="lista">
        <ul>
            <li><?=$a?></li>
            <li><?=$b?></li>
            <li><?=$c?></li>
            <li><?=(int)$d?></li>
        </ul>
    </div>    
</body>
</html>