<?php

/**
 * Funcion que calcula una operacion que es buscar 
 * el primer numero en el tercer argumento y el segundo numero en el tercer argumento
 * y devolver las veces que se repiten y su suma
 * @param mixed $datos cuyo primer y segundo argumento son dos numeros y el tercer argumento un string en formato csv
 * @return array $resultado array en cuya primera posicion esta las veces que se repite el primero numero
 * y en la segunda posicion las veces que se repite el segundo numero 
 * y en la tercera posicion la suma de ambos
 */
function operacion(...$datos)
{
    $resultado = [0, 0, 0];
    // buscamos el numero que esta en datos[0] dentro de datos[2]
    // almacenar el resultado en $resultado[0]
    $resultado[0] = substr_count($datos[2], $datos[0]);

    // buscamos el numero que esta en datos[1] dentro de datos[2]
    // almacenar el resultado en $resultado[1]

    $resultado[1] = substr_count($datos[2], $datos[1]);
    // sumar datos[0]+datos[1]
    // almacenar el resultado en $resultado[2]

    $resultado[2] = $datos[0] + $datos[1];

    return $resultado;
}

$resultado = [];

// controlamos si se ha pulsado el boton

// opcion 1
if (isset($_POST["boton"])) {

    // recogiendo los datos del formulario
    $num1 = $_POST["numero1"] ?: 0;
    $num2 = $_POST["numero2"] ?: 0;
    $operacion = $_POST["operacion"] ?: "";

    // llamamos a la funcion
    $resultado = operacion($num1, $num2, $operacion);
}

// opcion 2
// if ($_POST) {
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero1">Numero 1</label>
            <input type="number" id="numero1" name="numero1" required>
        </div>
        <div>
            <label for="numero2">Numero 2</label>
            <input type="number" id="numero2" name="numero2" required>
        </div>
        <div>
            <label for="operacion">Operacion</label>
            <input type="text" id="operacion" name="operacion" required>
        </div>
        <div>
            <button type="submit" name="boton">Enviar</button>
            <button type="reset">Limpiar</button>
        </div>
    </form>
    <?php
    if (isset($_POST["boton"])) {

    ?>
        <h2>Datos</h2>
        <p> El primer numero es <?= $num1 ?></p>
        <p> El segundo numero es <?= $num2 ?></p>
        <p> La operacion es <?= $operacion ?></p>

        <h2>Resultados</h2>
        <p>El primer numero <?= $num1 ?> se repite <?php echo $resultado[0] ?> veces</p>
        <p>El segundo numero <?= $num2 ?>se repite <?php echo $resultado[1] ?> veces</p>
        <p>La suma de ambos es <?php echo $resultado[2] ?></p>
    <?php
    }
    ?>
</body>

</html>
