<?php
//controlar que he pulsado el boton de enviar
if (isset($_POST["boton"])) {
    // Add your code here to handle the button click
    var_dump($_POST);
    $nombre=$_POST["nombre"] ?: "";
    $apellidos=implode(" ", $_POST["apellidos"]);
    //controlo si no he seleccionado nada con un ternario nulable
    $estado=$_POST["estado"] ?? "";
    //ternario para los checkbox si no se ha seleccionado nada 
    $aficiones= isset($_POST["aficiones"]) ? implode(", ", $_POST["aficiones"]) : "";

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="nombre">Nombre :</label>
            <input type="text" name="nombre" id="nombre">
        </div>
        <div>
            <label for="apellidos1">Apellidos :</label>
            <input type="text" name="apellidos[]" id="apellidos1">
        </div>
        <div>
            <label for="apellidos2">Apellidos :</label>
            <input type="text" name="apellidos[]" id="apellidos2">
        </div>
        <!-- arreglado -->
        <div>
            <span>Estado civil</span><br>
            <label for="casada">Casado/a</label>
            <input type="radio" name="estado" id="casado" value="casado">
            <label for="soltera">Soltero/a</label>
            <input type="radio" name="estado" id="soltero" value="soltero">
        </div>
        
        <div>
            <label for="deportes">Deportes</label>
            <input type="checkbox" name="aficiones[]" id="deportes" value="deportes">
            <label for="leer">Lectura</label>
            <input type="checkbox" name="aficiones[]" id="leer" value="leer">
            <label for="dormir">Dormir</label>
            <input type="checkbox" name="aficiones[]" id="dormir" value="dormir">
        </div>
        <div>
            <button name="boton">Enviar</button>
            <button type="reset">Borrar</button>
        </div>
    </form>
    <?php
        if($_POST){
        
        
    ?>
    <div>
        <h2>Datos</h2>
        <p> El nombre es <?= $nombre ?></p>
        <p> Los apellidos son <?= $apellidos ?></p>
        <p> El estado civil es <?= $estado ?></p>
        <p> Las aficiones son <?= $aficiones ?></p>
    </div>
    <?php
        }
    ?>
</body>
</html>