<?php
//controlar que he pulsado el boton de enviar
if ($_POST) {
    $email=$_POST["email"] ?: "";
    $password=$_POST["password"] ?: "";
    $mes=$_POST["mes"] ?: "";
    $acceso=isset($_POST["acceso"]) ? implode(", ", $_POST["acceso"]) : "No se ha seleccionado acceso";
    $ciudad=$_POST["ciudad"] ?: "";
    $navegadores=isset($_POST["navegadores"]) ? implode(", ", $_POST["navegadores"]) : "No hay navegadores seleccionado";
    // Add your code here to handle the button click
    var_dump($_POST);
    
}

//var_dump($_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div id="contenedor">   


    
    <form method="post">
        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" required placeholder="Email">
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" required placeholder="Password">
        </div>
        <div>   
            <label for="mes">Mes de acceso</label>
            <input type="radio" name="mes" id="enero" value="enero" checked>Enero
            <input type="radio" name="mes" id="febrero" value="febrero">Febrero
            <input type="radio" name="mes" id="marzo" value="marzo">Marzo
        </div>
        <div>
            <label for="formasA">Formas de acceso</label>
            <input type="checkbox" name="acceso[]" id="telefono" value="telefono">Telefono
            <input type="checkbox" name="acceso[]" id="ordenador" value="ordenador">Ordenador
        </div>
        <div>
            <label for="ciudad">Ciudad</label>
            <select name="ciudad"  required>
                <option value="" disabled selected>Seleciona una ciudad</option>
                <option value="madrid">Madrid</option>
                <option value="barcelona">Barcelona</option>
                <option value="valencia">Valencia</option>
            </select>
        </div>
        <div>
            <label for="navegadores">Navegadores utilizados</label>
            <select name="navegadores[]" id="navegadores" multiple size="3">
                <option  disabled selected>Seleciona navegadores</option>
                <option value="safari">Safari</option>
                <option value="google">Google Chrome</option>
                <option value="firefox">Firefox</option>
                <option value="edge">Edge</option>
            </select>
        </div>

            <div>
                <button type="submit" >Enviar</button>
                <button type="reset">Limpiar</button>
            </div>


    </form>
    </div>

    <?php
        if($_POST){
        ?>
        <div>
            <h2>Email</h2>
            <div><?= $email ?></div>
        <div>
            <div>
                <h2>Password</h2>
                <div><?= $password ?></div>
            </div>   
            <div>
                <h2>Mes de acceso</h2>
                <div><?= $mes ?></div>
            </div>
            <div>
                <h2>Formas de acceso</h2>
                <div><?= $acceso ?></div>
            </div>
            <div>
                <h2>Ciudad</h2>
                <div><?= $ciudad ?></div>
            </div>
            <div>
                <h2>Navegadores utilizados</h2>
                <div><?= $navegadores ?></div>
            </div>
            <?php
            }
            ?>
        
        
</body>
</html>