<?php
// el post esta vacio
var_dump($_POST);

// el files contiene los metadatos de los ficheros subidos
var_dump($_FILES);

// inicializo las variables

// con array asociativo

$ficheros = [
    "pdf" => [
        "mensaje" => "",
        "archivos" => [],
        "rutasDestino" => [],
        "ficheroSubido" => [],
    ],
    "imagen" => [
        "mensaje" => "",
        "archivos" => [],
        "rutasDestino" => [],
        "ficheroSubido" => [],
    ],
];


// compruebo si he pulsado el boton de enviar
if ($_FILES) {
    // creo un array con todos los metadatos del fichero
    $ficheros["pdf"]["archivos"] = $_FILES["pdf"];
    $ficheros["imagen"]["archivos"] = $_FILES["imagen"];
    // si queremos un array asociativo 
    // $archivos = [
    //     "pdf" => $_FILES["pdf"],
    //     "imagen" => $_FILES["imagen"]
    // ];

    // ruta donde guardo los archivos subidos dentro del servidor

    $ficheros["pdf"]["rutasDestino"] = "./pdf/" . $ficheros["pdf"]["archivos"]["name"];
    $ficheros["imagen"]["rutasDestino"] = "./imgs/" . $ficheros["imagen"]["archivos"]["name"];
    
    // $rutasDestino = [
    //     "pdf" => "./pdf/" . $archivos["pdf"]["name"],
    //     "imagen" => "./imgs/" . $archivos["imagen"]["name"] // $imagen["name"]
    // ];

    // para mover el archivo de la carpeta temporal
    // donde lo coloca el servidor a la ruta destino

    $ficheros["pdf"]["ficheroSubido"] = move_uploaded_file($ficheros["pdf"]["archivos"]["tmp_name"], $ficheros["pdf"]["rutasDestino"]);
    $ficheros["imagen"]["ficheroSubido"] = move_uploaded_file($ficheros["imagen"]["archivos"]["tmp_name"], $ficheros["imagen"]["rutasDestino"]);
    // $ficherosSubidos = [
    //     "pdf" => move_uploaded_file($archivos["pdf"]["tmp_name"], $rutasDestino["pdf"]),
    //     "imagen" => move_uploaded_file($archivos["imagen"]["tmp_name"], $rutasDestino["imagen"])
    // ];

        if($ficheros["pdf"]["ficheroSubido"]){
            $ficheros["pdf"]["mensaje"] = "Se ha subido el pdf correctamente";
        }else{
            $ficheros["pdf"]["mensaje"] = "No se ha podido subir el pdf";
        }

        if($ficheros["imagen"]["ficheroSubido"]){
            $ficheros["imagen"]["mensaje"] = "Se ha subido la imagen correctamente";
        }else{
            $ficheros["imagen"]["mensaje"] = "No se ha podido subir la imagen";
        }

        //TERNARIO

        // $fichero["imagen"]["mensaje"]= $ficheros["imagen"]["ficheroSubido"] ? "Se ha subido la imagen correctamente" : "No se ha podido subir la imagen";
        // $ficheros["pdf"]["mensaje"]= $ficheros["pdf"]["ficheroSubido"] ? "Se ha subido el pdf correctamente" : "No se ha podido subir el pdf";


    // if ($ficherosSubidos["pdf"]) {
    //     $mensaje["pdf"] = "Se ha subido el pdf correctamente";
    // } else {
    //     $mensaje["pdf"] = "No se ha podido subir el pdf";
    // }

    // if ($ficherosSubidos["imagen"]) {
    //     $mensaje["imagen"] = "Se ha subido la imagen correctamente";
    // } else {
    //     $mensaje["imagen"] = "No se ha podido subir la imagen";
    // }

    var_dump($ficheros);
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="pdf">
        <br>
        <input type="file" name="imagen">
        <br>
        <button>Enviar</button>
    </form>

    <div>
        <?= $ficheros["pdf"]["mensaje"] ?>
    </div>
    <div>
        <?= $ficheros["imagen"]["mensaje"] ?>
    </div>


</body>

</html>
