<?php
namespace src;
class Empleado extends Persona {
    private float $sueldo;

    //constructor
    public function __construct(string $nombre, int $edad, float $sueldo){
        parent::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }

    //metodos

    public function calcularSueldo(int $horas){
        return $this->sueldo = 10 * $horas;
        
    }

    //mostrar
    public function mostrar(){
        parent::mostrar();
        echo "y mi sueldo es $this->sueldo";
    }

}