<?php
namespace src;

class Directivo extends Empleado{
    private string $categoria;

    public function __construct(string $nombre, int $edad, float $sueldo){
        parent::__construct($nombre, $edad, $sueldo);
        $this->categoria = "Directivo";
    }

    //metodos

    public function mostrar(){
        parent::mostrar();
        echo "y mi categoria es $this->categoria";
    }

    // getters y setters

    public function getCategoria(){
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self{
        $this->categoria = $categoria;
        return $this;
    }
}