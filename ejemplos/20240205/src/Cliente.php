<?php
namespace src;
class Cliente extends Persona{
    private string $nombreEmpresa;
    private string $telefono;

    public function __construct(string $nombre, int $edad, string $nombreEmpresa, string $telefono){
        parent::__construct($nombre, $edad);
        $this->nombreEmpresa = $nombreEmpresa;
        $this->telefono = $telefono;
    }

    public function mostrar(){
        parent::mostrar();
        echo "y mi nombre de empresa es $this->nombreEmpresa y mi telefono es $this->telefono";
    }

    public function getNombreEmpresa(){
        return $this->nombreEmpresa;
        
    }

    public function setNombreEmpresa(string $nombre): self{
        $this->nombreEmpresa = $nombre;
        return $this;
    }

    public function getTelefono(){
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self{
        $this->telefono = $telefono;
        return $this;
    }
}

