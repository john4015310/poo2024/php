<?php
namespace src;
class Persona{
    //atributos
    private string $nombre;
    private int $edad;
    //constructor
    public function __construct(string $nombre, int $edad){
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    //getters y setters
    public function getNombre(){
        return $this->nombre;
    }
    public function getEdad(){
        return $this->edad;
    }
    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;
        return $this;
    }
    public function setEdad(int $edad): self{
        $this->edad = $edad;
        return $this;
    }

    //metodos
    public function mostrar():string{
        $salida = "<ul>";
        $salida .= "<li>Nombre: $this->nombre</li>";
        $salida .= "<li>Edad:  $this->edad </li>";
        $salida .= "</ul>";
        return $salida;
    }

}