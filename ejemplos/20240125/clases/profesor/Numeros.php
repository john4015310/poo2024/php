<?php
namespace clases\profesor;
class Numeros{
    public array $numeros;
    public function __construct(array $numeros=[]){
        $this->numeros = $numeros;
    }

    public function calcularModa():float{
        //que tenga en cuenta que puede haber mas de 1 numero con el mismo numero de repeticiones
        $frecuenciaMaxima = 0;
        $valoresMaximasRepeticiones=[];
        $moda=0;


        $frecuencias=array_count_values($this->numeros);
        foreach ($frecuencias as $indice => $frecuencia) {
            # code...
            if($frecuencia > $frecuenciaMaxima){
                $frecuenciaMaxima = $frecuencia;
                
            }
        }
        //-------------------------------------------------
        foreach ($frecuencias as $indice => $frecuencia) {
            # code...
            if($frecuencia == $frecuenciaMaxima){
                $valoresMaximasRepeticiones[]=$indice;
            }
        }

        $moda= array_sum($valoresMaximasRepeticiones)/count($valoresMaximasRepeticiones);

        return $moda;
    }
}