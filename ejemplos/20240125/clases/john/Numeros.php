<?php
namespace clases\john;
class Numeros{
    public array $numeros;
    public function __construct(array $numeros=[]){
        $this->numeros = $numeros;
    }

    //metodo capaz de calcular la media

    public function media():float{
        $suma = 0;
        foreach($this->numeros as $num){
            $suma += $num;
        }
        return $suma / count($this->numeros);
    }

    //calculaar la media y calcular la moda

    public function moda():float{
        $frecuenciaMaxima = 0;
        $moda=0;
        $frecuencias=array_count_values($this->numeros);
        foreach ($frecuencias as $indice => $frecuencia) {
            # code...
            if($frecuencia > $frecuenciaMaxima){
                $frecuenciaMaxima = $frecuencia;
                $moda = $indice;
            }

        }
        //opcion 2
        // $frecuencias = array_count_values($this->numeros);
        // $frecuenciaMaxima= max($frecuencias);
        // $moda= array_search($frecuenciaMaxima, $frecuencias);
        

        return $moda;
    }

    public function calcularMedia():float{
        $media=0;
        $media= array_sum($this->numeros) / count($this->numeros);
        return $media;
    }
}