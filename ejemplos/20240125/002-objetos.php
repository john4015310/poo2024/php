<?php
//voy a utilizar espacios de nombres
//para tener la misma clase en varias carpetas
//para poder tener organizadas mis clases

//coloco la auto carga de clases
spl_autoload_register(function($clase){
    include $clase .'.php';
});

$objeto1= new clases\profesor\Numeros([2,3,5]);

var_dump($objeto1);

$objeto2= new clases\john\Numeros([1,2,4,4,2,4]);
var_dump($objeto2);

echo "La media del array es: ".$objeto2->media();