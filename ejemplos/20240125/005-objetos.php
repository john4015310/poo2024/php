<?php
//importo la clase al espacio de nobres actual
use clases\profesor\Texto;
//para no tener errores al estar instanciando las dos clases con el nombre txto
//usamos el alias
use clases\john\Texto as TextoJohn;
spl_autoload_register(function($clase){
    include $clase.".php";
});

//crear un onbjeto de tipo texto

$objeto1= new Texto();
$objeto2= new TextoJohn();

echo $objeto1->mostrar();
echo "<br/>";
echo $objeto2->mostrar();