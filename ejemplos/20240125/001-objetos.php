<?php
//precarga de clases
spl_autoload_register(function ($clase) {
    require "clases/". $clase . '.php';
});

//creo un objeto de tipo coche 

$ford1 = new Coche();

//creo otro obbeto de tipo coche

$ford2 = new Coche('black', 5, 'ford');

//si quiero poner la marca de ford en el objeto ford1
//$ford1->marca='ford';
$ford1->setMarca('ford');
//$ford1->gasolina=10; //da error por q es privado
$ford1->setGasolina(10);
echo $ford1->getGasolina();
$ford2->llenarTanque(10);

var_dump($ford1);
var_dump($ford2);