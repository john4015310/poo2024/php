<?php

    /**
     * crear una variable llamada numero que puede valer del 1 - 100 numero entero
     * imprimir
     * "alpe" si el numero es multiplo de 3
     * "POO" si el numero es multiplo de 5
     * "alpePOO" si el numero es multiplo de 3 y 5
     */

    $numero=(int) mt_rand(1,100);
    $salida = "";

    switch($numero){
        case $numero%3==0 && $numero%5==0:
            echo "{$numero}<br>alpePOO";
            break;
        case $numero%3==0:
            echo "{$numero}<br>alpe";
            break;
        case $numero%5==0:
            echo "{$numero}<br>POO";
            break;
        default:
            echo "El número {$numero} no es multiplo de 5 o 3 ";
    }

    