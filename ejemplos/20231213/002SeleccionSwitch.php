<?php

    $letra=["r","v","A"][mt_rand(0,2)];
    $salida = "";

    switch($letra){
        case "r":
            $salida= "Rojo";
            break;
        case "v":
            $salida= "Verde";
            break;
        case "A":
            $salida= "Azul";
            break;
        default:
            $salida= "No hay coincidencias";

    }

    echo $salida;