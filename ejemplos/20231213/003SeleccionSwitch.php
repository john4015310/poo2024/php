<?php
    /**
     * crear una variable llamada unidades que puede valer 
     * entre uno y cien 
     * imprimir descuento que se aplica a la compra en funcion de unidades solicitadas
     * 
     * realizarlo con switch
     * 
     * <10 1%
     * 10 y <50 2%
     * >=50 10%
     */

    $unidades = 0;
    $salida = "";

    $unidades = mt_rand(1,100);

    switch (true) {
        case ($unidades < 10):
            $salida = "La compra tiene un descuento del 1%";
            break;
        case ($unidades < 50):
            $salida = "La compra tiene un descuento del 2%";
            break;
        case ($unidades >= 50):
            $salida = "La compra tiene un descuento del 10%";
            break;    
    }

    echo "Por la compra de $unidades se aplica un descuento de:<br>";
    echo $salida;

