<?php
use src\Cliente;
use src\Empleado;
use src\Directivo;
require "autoload.php";

$empleados = [];

$empleados [0] = new Empleado();
$empleados[0]->setNombre("John");
$empleados[0]->setEdad(37);
$empleados[0]->calcularSueldo(54);

echo $empleados[0]->mostrar();

$empleados [1] = new Empleado();
$empleados[1]->setNombre("Constantine");
$empleados[1]->setEdad(37);
$empleados[1]->calcularSueldo(45);

echo $empleados[1]->mostrar();

$clientes = [];
$clientes[0] = new Cliente();
$clientes[0]->setNombre("John");
$clientes[0]->setEdad(37);
$clientes[0]->setNombreEmpresa("Microsoft");
$clientes[0]->setTelefono("123456789");

echo $clientes[0]->mostrar();


$directivos = [];
$directivos[0] = new Directivo();
$directivos[0]->setNombre("Jose");
$directivos[0]->setEdad(30);
$directivos[0]->calcularSueldo(5);
$directivos[0]->setCategoria("A");
echo $directivos[0]->mostrar();
