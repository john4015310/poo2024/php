<?php
namespace src;
require "autoload.php";
class Directivo extends Empleado
{
   private ?string $categoria;

   public function mostrar():string{
    $salida = "<ul>";
    $salida .= "<li>Nombre" . $this->getNombre() . "</li>";
    $salida .= "<li>Edad" . $this->getEdad() . "</li>";
    $salida .= "<li>Salario" . $this->getSueldo() . "</li>";
    $salida .= "<li>Categoría" . $this->categoria . "</li>";
    $salida .= "</ul>";
    return $salida;
   }

   /**
    * Get the value of categoria
    *
    * @return ?string
    */
   public function getCategoria(): ?string
   {
      return $this->categoria;
   }

   /**
    * Set the value of categoria
    *
    * @param ?string $categoria
    *
    * @return self
    */
   public function setCategoria(?string $categoria): self
   {
      $this->categoria = $categoria;

      return $this;
   }
}