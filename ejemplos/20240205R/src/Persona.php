<?php
namespace src;
class Persona{
    //atributos
    private ?string $nombre;
    private ?int $edad;
    //constructor
    public function __construct(){
        $this->nombre = null;
        $this->edad = null;
    }

    //getters y setters
   

    //metodos
    public function mostrar():string{
        $salida = "<ul>";
        $salida .= "<li>Nombre: $this->nombre</li>";
        $salida .= "<li>Edad:  $this->edad </li>";
        $salida .= "</ul>";
        return $salida;
    }

    public function __toString(){
        return $this->mostrar();
    }

    /**
     * Get the value of nombre
     *
     * @return ?string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @param ?string $nombre
     *
     * @return self
     */
    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of edad
     *
     * @return ?int
     */
    public function getEdad(): ?int
    {
        return $this->edad;
    }

    /**
     * Set the value of edad
     *
     * @param ?int $edad
     *
     * @return self
     */
    public function setEdad(?int $edad): self
    {
        $this->edad = $edad;

        return $this;
    }
}