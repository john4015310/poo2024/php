<?php

$dato = [
    "nombre" => "Pepe",
    "apellidos" => null,
    "edad" => null
];

//quiero realizarlo con el ternario recortado
//


//quiero realizar lo mismo pero con ternario
// $nombre="Pepe";
// $apellidos="Perez";
// $edad="no conocida";

// $nombre = $dato["nombre"] ?? "no conocido";
// $apellidos = $dato["apellidos"] ?? "no conocido";
// $edad = $dato["edad"] ?? "no conocido";

//quiero realizarlo con el operador recortado para elvis
//var_dump($nombre, $apellidos, $edad);

$nombre = $dato["nombre"] ?: "no conocido";
$apellidos = $dato["apellidos"] ?: "no conocido";
$edad = $dato["edad"] ?: "no conocido";
var_dump($nombre, $apellidos, $edad);


