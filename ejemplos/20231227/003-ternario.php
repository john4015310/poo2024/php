<?php

$dato = [
    "nombre" => "Pepe",
    "apellidos" => null,
    "edad" => null
];

//utilizando el operador if/else
//junto con la funcion isset
//realizar las siguientes asignaciones


//quiero realizar lo mismo pero con ternario
// $nombre="Pepe";
// $apellidos="Perez";
// $edad="no conocida";

// if(isset($dato["nombre"])){
//     $nombre = $dato["nombre"];
    
// }else{
//     $nombre = "no conocido";
// }
// if(isset($dato["apellidos"])){
//     $apellidos = $dato["apellidos"];
    
// }else{
//     $apellidos = "no conocido";
// }
// if(isset($dato["edad"])){
//     $edad = $dato["edad"];
    
// }else{
//     $edad = "no conocido";
// }


// foreach($datos as $key => $value){
//     if(isset($value)){
//         $nombre = $value["nombre"];
//         $apellidos = $value["apellidos"];
//         $edad = $value["edad"];
//     }
// }

$nombre = isset($dato["nombre"]) ? $dato["nombre"] : "no conocido";
$apellidos = isset($dato["apellidos"]) ? $dato["apellidos"] : "no conocido";
$edad = isset($dato["edad"]) ? $dato["edad"] : "no conocido";



var_dump($nombre, $apellidos, $edad);
var_dump($dato);