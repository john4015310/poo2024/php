<?php
//crear una funcion que reciba como promer rgumento una etiqueta html
// y como segunargumento un contenido de etiqueta
//debe devolver una etiqueta html con el ontenido
//de la etiqueta

/**
 * Crea una etiqueta HTML con el nombre de etiqueta y contenido especificados.
 *
 * @param string $etiqueta - El nombre de la etiqueta HTML.
 * @param string $contenido - El contenido que se colocará dentro de la etiqueta.
 * @return string - La etiqueta HTML con el nombre de etiqueta y contenido especificados.
 */
function crearEtiqueta(string $etiqueta, string $contenido):string{

    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("h1","Hola");