<?php

function crearEtiqueta($contenido, $etiqueta,...$atributos){
    $resultado="<{$etiqueta} ";
    foreach($atributos as  $atributo){
        $resultado.= "{$atributo} ";
    }
    $resultado.=">{$contenido}</{$etiqueta}>";
    return $resultado;
    
}

echo crearEtiqueta("Hola mundo","h1","id='red'");

//si lo queremos hacer diferente si no ponemos los tres puntos en atributos los atributos se lo pueden pasar uno por uno en la llamada de la funcion
// pero si no se ponen los tres puntos en atributos los atributos se lo pasan en un array
// y en la funcion pondriamos los atributos entre corchetes para mostrar el array 
