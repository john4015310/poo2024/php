<?php

/**
 * Crea un elemento HTML con el contenido y etiqueta especificados.
 *
 * @param string $contenido El contenido del elemento.
 * @param string $etiqueta La etiqueta del elemento. Por defecto es 'div'.
 * @return string El elemento HTML.
 */

function crearEtiqueta($contenido,$etiqueta="div"){
    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("Hola mundo");
echo crearEtiqueta("Hola mundo","h1");
//echo crearEtiqueta();