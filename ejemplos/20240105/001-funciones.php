<?php
//funcion declarada
function ejemplo(){
    return 1;
}
//funcion declarada con un argumento
function ejemplo1($a){
    return $a;
}

//llamar funcion
echo ejemplo();
echo ejemplo(1,2,3);
echo ejemplo1("a");
echo ejemplo1("a","b"); //es normal pasar un argumento pero se pueden poner mas 