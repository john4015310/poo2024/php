<?php
session_start();

$mensaje="";
//inicializamos variables de session
//compruebo si existe la variable de session
if(!isset($_SESSION["aciertos"])){
    $_SESSION["numeros"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $_SESSION["aciertos"]=[];
    $_SESSION['errores'] = 0;
    $_SESSION['intentos'] = 0;
}else{
    //estoy cargando la pagina la n veces que hag falta
    if($_POST){
        //comprobar que el numero enviado esta dentro del array numeros
        if(
            in_array($_POST["numero"], $_SESSION["numeros"])
            &&
            !in_array($_POST['numero'], $_SESSION['aciertos'])
            ){
            $_SESSION['aciertos'][]=$_POST['numero']; //asi de esta forma agregamos el numero al array poniendo los corchetes despues de la variable
            
            if(count($_SESSION['aciertos']) == 10 ){
                session_destroy();
                $mensaje ="has ganado";
            }

        }else{
            //si no he acertado el numero sumo los errores
            $_SESSION["errores"]++;
            
        }
        //tanto acieerto como si fallo
        //sumo uno al contador de intentos

        $_SESSION["intentos"]++;

    }
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero"></label>
            <input type="number" name="numero" id="numero" required>
        </div>
        <div>
            <button type="submit">Jugar</button>
        </div>
    </form>
    <?=var_dump($_SESSION)?>
</body>
</html>