<?php
session_start();
// Crear un formulario que permita introducir un numero
// almacenar ese numero en una variable de sesion
// y mostrarlo por pantalla
    if($_POST){
        $numero=$_POST['numero1'];
        $_SESSION['numero'] = $numero;
        var_dump($_SESSION);
    }
    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
        <form method="post" >
                <div class="mb-3">
                  <label for="numero1" class="form-label">Número 1 </label>
                  <input type="number" class="form-control" id="numero1" name="numero1" placeholder="Introducir un número" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>