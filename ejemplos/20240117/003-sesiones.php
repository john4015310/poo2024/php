<?php
session_start();
//quiero un formulario que me permita introducir un numero
//y quiero que me muestre todos lo numeros introducidos por el usuario
//realizar ejejrciio con sesiones

$numero = [];
//var_dump($_SESSION);

//he pulsado el boton enviar
if($_POST){
    //comprombamos si existe la variable d session
    if(isset($_SESSION['numero'])){
        $numero = $_SESSION['numero'];

    }else{
        $numero=[];
    }
    //$numero = $_SESSION['numero']?? [];
    array_push($numero, $_POST['numero1']);
    $_SESSION['numero'] = $numero;

}

//otro forma
//------------------------------------
// if (!isset($_SESSION['numero'])) {
//     $_SESSION['numero'] = [];
// }
// if($_POST){

//     array_push($_SESSION['numero'], $_POST['numero1']);
//     //$_SESSION['numero'][] = $_POST['numero1'];
// }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

    <div class="container">
        <form method="post" >
                <div class="mb-3">
                  <label for="numero1" class="form-label">Número 1 </label>
                  <input type="number" class="form-control" id="numero1" name="numero1" placeholder="Introducir un número" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>


                
        </form>
        <br>
        <div class="card mb-3">
                        <div class="card-header">
                          Números
                        </div>
                        <div class="card-body">
                          <?php
                            if($_POST){
                              
                            foreach ($_SESSION['numero'] as $key) {
                            ?>
                            <p class="card-text"><?=$key?></p>
                            <?php
                            }
                          }
                          ?>
                          
                          

                          
                        </div>
                    </div>
        
    </div>
    

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>