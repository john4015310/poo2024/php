<?php
session_start();

// comprobar si he pulsado el boton de enviar
if ($_POST) {
    $_SESSION['datos'] = [
        'nombre' => $_POST['nombre'],
        'apellidos' => $_POST['apellidos']
    ];
}

// si no existe la variable de session redirecciono al formulario
if (!isset($_SESSION['datos'])) {
    //sirve para redireccionar al fomulario por que no hay valores que mostrar por pantalla
    header('Location: 007-sessiones.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formularios.css">

</head>

<body>
    <h1>Datos introducidos</h1>
    <div class="etiqueta">
        <span class="etiqueta">Nombre </span> : <?= $_SESSION['datos']['nombre'] ?>
    </div>
    <br>
    <div class="etiqueta">
        <span class="etiqueta">Apellidos </span> : <?= $_SESSION['datos']['apellidos'] ?>
    </div>
    <br>
    
    <div>
        <a class="boton" href="007-sessiones.php">Volver</a>
    </div>
</body>

</html>
