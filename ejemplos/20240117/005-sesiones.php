<?php

// inicializar la sesion
session_start();

// se trata de realizar un pequeño juego donde
// el usuario tiene que adivinar los 10 numeros del array numeros
// que debo crear con 10 numeros aleatorios entre 1 y 50
// creamos un formulario donde escribo un numero y cuando pulso
// enviar debe comprobar si el numero esta en el array
// si esta en el array me indica que he acertado y me lo muestra
// si fallo me cuenta un fallo y me debe mostrar el numero de fallos
// cuando adivine los 10 numeros la puntuacion sera el numero de intentos 
// para adivinar los 10 numeros

// una vez que adivina los 10 numeros debe borrar las variables de sesion
// colocar un boton para reiniciar el juego cuando quieras

// function aleatorios(){
//     $resultado=[];
//     for ($i=0; $i < 10; $i++) { 
//         $resultado[] = rand(1, 50);
//     }
//     return $resultado;
// }

function aleatorios(){
    //crea un array con los primeros 50 numeros
    $numeros= range(1, 50);
    //mezcla el array
    shuffle($numeros);
    //me quedo con los primeros 10
    return array_slice($numeros, 0, 10);
}

if(!isset($_SESSION["numeros"])){
    $_SESSION["numeros"] = aleatorios();
    $_SESSION["aciertos"]=[];
    $_SESSION['errores'] = 0;
    $_SESSION['intentos'] = 0;
}else{
    if(isset($_POST["jugar"])){

        if(isset($_POST["numero"])){
            $numero=$_POST["numero"];
            if(in_array($numero, $_SESSION["numeros"]) && !in_array($numero, $_SESSION["aciertos"])){
                $_SESSION['aciertos'][]=$numero;
                
                unset($_SESSION['numeros'][array_search($numero, $_SESSION['numeros'])]);
                if(count($_SESSION['aciertos']) == 10 ){
                    session_destroy();
                    $mensaje ="has ganado";
                }
    
            }else{
                $_SESSION['errores']++;
                
            }
            $_SESSION['intentos']++;     
    }
    
    
    
    }
}







var_dump($_SESSION);
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <div>
            <label for="numero"></label>
            <input type="number" name="numero" id="numero" required>
        </div>
        <div>
            <button type="submit" name="jugar">Jugar</button>
            <button type="submit" name="reiniciar">Jugar</button>
        </div>
    </form>
    
    
</body>
</html>