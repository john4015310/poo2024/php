<?php

$parametros = require_once("parametros.php");
require_once("funciones.php");

//heinicializado la variable que muestra
//el mensaje de si ha sido correcta
//la insercion
$salida = "";
$accion = "Insertar";
//he inicializado el array de datos
//que me muestra esto em el formulario
$datos = [
    "titulo" => "",
    "paginas" => "",
    "fechaPublicacion" => ""
];
//desactivar errores
controlErrores();

//conexion a base de datos
//comprobar si he pulsado el boton de insertar
if($_POST){
    //si he pulsado el botn leo los datos que envio del formulario
    $datos["titulo"] = $_POST["titulo"];
    $datos["paginas"] = $_POST["paginas"];
    $datos["fechaPublicacion"] = $_POST["fechaPublicacion"];
    

//conexión de la base de datos
$conexion = @new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);

//compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("La conexión ha fallado: " . $conexion->connect_error);
}


$sql= "INSERT INTO libros 
(titulo, paginas, fechaPublicacion) VALUES 
(
    '{$datos["titulo"]}',
     {$datos["paginas"]},
    '{$datos["fechaPublicacion"]}'
)";


if($conexion->query($sql)){
    $salida= "Registro insertado correctamente";
    $salida= "<br><br><a href='index.php'>Ver todos los registros</a>";
}else{
    $salida= "Error: " . $sql . "<br>" . $conexion->error;
}

$conexion->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$parametros["nombreAplicacion"]?></title>
</head>
<body>
    <h1><?=$parametros["aplicacion"]["nombreAplicacion"]?></h1>
    <div>
        <?php
        require_once("_nav.php");
        echo "<hr>";
        require_once("_form.php");
        ?>
    </div>
    <div>
        <?=$salida?>
    </div>
</body>
</html>