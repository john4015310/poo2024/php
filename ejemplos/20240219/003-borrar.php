<?php

$parametros = require_once("parametros.php");
require_once("funciones.php");

//desactivar errores
controlErrores();

if(!isset($_GET["id"])){
    header("Location: index.php");
    exit;
}

//conexion a base de datos

$conexion = @new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);

//compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("La conexión ha fallado: " . $conexion->connect_error);
}

//el registro a borrar me llega por get
$sql= "DELETE  FROM libros WHERE id =". $_GET["id"];


if($conexion->query($sql)){
    $salida="Registro eliminado correctamente";
    $salida="<a href='index.php'>Listar Registros</a>";

}else{
    $salida="Error al eliminar el registro: "  . $conexion->error;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$parametros["aplicacion"]["nombreAplicacion"]?></title>
</head>
<body>
    <h1><?=$parametros["aplicacion"]["nombreAplicacion"]?></h1>
    <div>
        <?php
        require_once("_nav.php");
        ?>
    </div>
    <div>

        <?=$salida?>
    </div>
</body>
</html>