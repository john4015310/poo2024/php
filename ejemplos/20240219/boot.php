<?php
    $parametros = require_once("parametros.php");
    require_once("funciones.php");
    
    //desactivar errores
    controlErrores();
    
    //conexion a base de datos
    
    $conexion = @new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);
    
    //compruebo si la conexion es correcta
    if ($conexion->connect_error) {
        die("La conexión ha fallado: " . $conexion->connect_error);
    }

    $sql= "SELECT * FROM libros";

    if($resultados=$conexion->query($sql)){
        $salida= gridViewBotones($resultados);
       
    }else{
        $salida="Error al ejecutar la consulta: "  . $conexion->error;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
<div class="container">
    <h1><?=$parametros["aplicacion"]["nombreAplicacion"]?></h1>
    <?php require_once("_navBoot.php"); ?>
    <div><?=$salida ?></div>

</div>





<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>