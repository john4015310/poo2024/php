<?php
//arranco la session para almacenar el id a modificar
session_start();

$parametros = require_once "parametros.php";

require_once "funciones.php";

// desactivar errores
controlErrores();

$salida= "";
$accion = "Actualizar";
// conexion a base de datos
$conexion = @new mysqli(
    $parametros["bd"]["servidor"],
    $parametros["bd"]["usuario"],
    $parametros["bd"]["password"],
    $parametros["bd"]["nombreBd"]
);
// compruebo si la conexion es correcta
if ($conexion->connect_error) {
    die("Error de conexión: " . $conexion->connect_error);
}

if($_POST){
    //leer todos los datos del formulario
    //si he colocadoun campo hidden en el formulario    
    //$datos["id"] = $_POST["id"];
    $datos["id"] = $_SESSION["id"];
    $datos["titulo"] = $_POST["titulo"];
    $datos["paginas"] = $_POST["paginas"];
    $datos["fechaPublicacion"] = $_POST["fechaPublicacion"];

    $sql = "UPDATE libros 
        SET 
            titulo = '" . $datos["titulo"] . "',
            paginas = " . $datos["paginas"] . ",
            fechaPublicacion = '" . $datos["fechaPublicacion"] . "'
            WHERE id = " . $datos["id"];

    if ($conexion->query($sql)) {
        $salida = "Registro actualizado correctamente";
        $salida .= "<a href='index.php'>Listar Registros</a>";
    } else {
        $salida = "Error al actualizar el registro: " . $conexion->error;
    }
}

//si vengo del listado y quiere mostrar 
if(isset($_GET["id"])){
    //almaceno en nuna variable de session el id
    //del registro a modificar
    $_SESSION["id"] = $_GET["id"];
    //preparo el texto de la consulta 
    $sql = "select * from libros where id=" . $_GET["id"];
    //ejecuto la consulta
    $resultados = $conexion->query($sql);
    //saco lo datos del registro a modificar
    $datos = $resultados->fetch_assoc();
}

$conexion->close();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $parametros["nombreAplicacion"] ?></title>
</head>

<body>
    <h1><?= $parametros["aplicacion"]["nombreAplicacion"] ?></h1>
    <div>
        <?php require "_nav.php" ?>
    </div>
    <div>
        <?= $salida ?>
    </div>
    <div>
        <?php require "_form.php" ?>
    </div>

</body>

</html>
