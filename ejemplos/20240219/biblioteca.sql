﻿-- Crear la base de datos "personas"
DROP DATABASE IF EXISTS biblioteca;

CREATE DATABASE biblioteca;

-- Usar la base de datos "personas"
USE biblioteca;

-- Crear la tabla "personas"
CREATE TABLE libros (
    id INT AUTO_INCREMENT,
    titulo VARCHAR(200) NOT NULL,
    paginas INT,
    fechaPublicacion DATE,
    PRIMARY KEY (id)


);

INSERT INTO
    libros (
        titulo, paginas, fechaPublicacion
    )
VALUES (
        'Luces de bohemia', 30, '1992-05-15'
    ),
    (
        'Crimen y castigo', 25, '1997-09-20'
    ),
    (
        '100 años de Soledad', 28, '1994-03-10'
    ),
    (
        'La casa de los espíritus', 22, '1999-07-05'
    ),
    (
        'Preludio a la fundación', 35, '1986-12-12'
    ),
    (
        'Las almas muertas', 42, '1979-04-03'
    ),
    (
        'La comedia humana', 31, '1990-08-18'
    ),
    (
        'Cumbres borrascosas', 27, '1994-05-22'
    ),
    (
        'La muerte de Artemio Cruz',  38, '1983-11-07'
    ),
    (
        'Fahrenheit 451', 29, '1992-02-14'
    ),
    (
        'Miguel', 33, '1988-06-28'
    );