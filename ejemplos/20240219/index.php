<?php
    $parametros = require_once("parametros.php");
    require_once("funciones.php");
    
    //desactivar errores
    controlErrores();
    
    //conexion a base de datos
    
    $conexion = @new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);
    
    //compruebo si la conexion es correcta
    if ($conexion->connect_error) {
        die("La conexión ha fallado: " . $conexion->connect_error);
    }
    //preparo la consulta
    $sql= "SELECT * FROM libros";

    //creo un objeto de tipo mysqli_result e la consulta
    if($resultados=$conexion->query($sql)){
        //crear una tabla con los registros
        $salida= gridViewBotones($resultados);
       
    }else{
        $salida="Error al ejecutar la consulta: "  . $conexion->error;
    }

    //elementos de Menu de Ramon en una funcion
    //creo un array con los elementos que quiero que tenga el menu
    $elementosMenu=[
        "Inicio" => "index.php",
        "Insertar" => "001-insertar.php",
    ];
    //muestro menu
    $menu = menu($elementosMenu);

    // necesito los libros

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <title>Aplicacion de Libros</title>
</head>
<body>
<h1><?=$parametros["aplicacion"]["nombreAplicacion"]?></h1>
    <div>
        <?php require_once("_nav.php"); ?>
    </div>
    <div><?=$salida ?></div>
    
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>