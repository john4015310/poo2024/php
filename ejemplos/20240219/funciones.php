<?php

function gridViewBotones(mysqli_result $resultados){
    if ($resultados->num_rows >0){

        $registros= $resultados->fetch_all(MYSQLI_ASSOC);
        $salida = "<table border = 1>";
        $campos = array_keys($registros[0]);
        foreach ($campos as $campo) {
            # code...
            $salida .= "<th>$campo</th>";
        }
        //añado columna de botones
        $salida .= "<th>Acciones</th>";
        $salida .= "<tr></thead>";
        foreach($registros as $registro){
            $salida .= "<tr>";
            foreach($registro as $clave => $valor){
                $salida .= "<td>$valor</td>";
            }
            $salida .= "<td><a href='002-actualizar.php?id=" . $registro['id']."'>Editar</a> ";
            $salida .= "<a href='003-borrar.php?id=" . $registro['id'] ."'>Eliminar</a>";
            $salida .= "</tr>";
        }
        $salida .= "</table>";
        }else{
            $salida="No hay registros";
        }
        return $salida;
}
    function controlErrores(){
        //desactivar errores en pantalla
        //error_reporting(0);
        // convertir los errroes sde la libreria mysqli en warnings
        mysqli_report(MYSQLI_REPORT_OFF);
    }


   function menu(array $elemento):string{
       $salida = "";
       $salida .= "<ul>";
       foreach ($elemento as $label => $enlace) {
        # code...
        $salida .= "<li><a href='{$enlace}'>{$label}</a></li>";
       }
       $salida .= "</ul>";
       return $salida;
   }