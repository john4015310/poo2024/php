﻿-- Lenguaje de Definicion de datos
# 
DROP DATABASE IF EXISTS personas;
CREATE DATABASE personas;
USE personas;
CREATE TABLE persona(
  idPersona int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  PRIMARY KEY(idPersona)
);
-- Lenguaje de manipulacion de datos

INSERT INTO persona 
(nombre, edad) VALUES 
('persona1', 18),
('persona2', 45);

/*
SELECT p.idPersona,
       p.nombre,
       p.edad FROM persona p;
*/
CREATE TABLE cliente(
  idcliente int AUTO_INCREMENT,
  nombre varchar(200),
  edad int,
  nombreEmpresa varchar(200),
  telefono varchar(100)
  PRIMARY KEY(idcliente)
  );

INSERT INTO cliente 
(nombre, edad, nombreEmpresa, telefono)  VALUES 
('Itadori', 21, 'Jujutsu Kaizen', '689221298'),
('Mash', 21, 'Masle an Magic', '606260154');