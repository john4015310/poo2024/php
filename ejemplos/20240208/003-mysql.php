<?php
//obejtivo

//conectarme alservidor de base de datos mysqli 
//selecionar la base de datos personas
// realizar una consulta para mostrar tabla
//mostrar resultado

//conectarme a la base de datos

$conexion = new mysqli(
    "localhost", // host al que quiero conectarme
    "root", // usuario de la base de datos
    "",//contraseña
    "personas" // basede datos por defecto y es opcional
);

//realizo la consulta a la base de datos
$resultados = $conexion->query("SELECT p.idPersona,p.nombre,p.edad FROM persona p;");

/**
 * quiero para sacar los datos crear solamente un array unidimensional
 */
//realizo el listado con un while y el metodo fech_array
 //para crear un array asosiativo con el primer registro
$contador=0;
// $registro= $resultados->fetch_array(MYSQLI_ASSOC);
//con el while va leyendo todos los registro y los pasa al array asosiativo


while ($registro= $resultados->fetch_array(MYSQLI_ASSOC)) {
    # code...
    echo "<fieldset>";
    echo "<legend>Registro : {$contador}</legend>";
    echo "<ul>";
    echo "<li>idPersona:$registro[idPersona]</li>";
    echo "<li>nombre:$registro[nombre]</li>";
    echo "<li>edad:$registro[edad]</li>";
    echo "</ul>";
    echo "</fieldset>";
    $contador++;
}



//colocar el puntero de los resultados al primer registro de nuevo
$resultados->data_seek(0);
//para ello utilizo el metodo dataseek
// puedo realizar lo mismo con un while y un foreach
$contador = 0;
while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo}: {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}


//cerramos conexion
$conexion->close();

// var_dump($registro);
