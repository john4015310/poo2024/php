<?php
// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// utilizamos el metodo FETCH_OBJECT
// mostrar los registros de la tabla persona
// cerrar conexión


$conexion = new mysqli(
    "localhost", // host al que quiero conectarme
    "root", // usuario de la base de datos
    "",//contraseña
    "personas" // basede datos por defecto y es opcional
);

$resultados = $conexion->query("SELECT * FROM persona;");

$contador = 0;

while ($persona = $resultados->fetch_object()) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    echo "<li>ID :{$persona->idPersona}</li>";
    echo "<li>NOMBRE :{$persona->nombre}</li>";
    echo "<li>EDAD :{$persona->edad}</li>";
    echo "</ul>";
    $contador++;
}



$resultados->data_seek(0);
//---------------------------------------------------------
while ($persona = $resultados->fetch_object()) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    foreach ($persona as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo}: {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}