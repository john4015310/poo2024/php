<?php

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla Cliente
// utilizamos el metodo FETCH_ARRAY
// mostrar los registros de la tabla cliente
// cerrar conexión


//conecto a la base de datos
$conexion = new mysqli(
    "localhost", // host al que quiero conectarme
    "root", // usuario de la base de datos
    "",//contraseña
    "personas" // basede datos por defecto y es opcional
);
//consulta

$resultados = $conexion->query("SELECT * FROM cliente;");

//mostrarlos registros
//recorrindo utilizamos el metodo fetch_array
$contador = 0;
while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo}: {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}

//cerramos conexion
$conexion->close();