<?php
session_start();
$parametros = require_once("parametros.php");
require_once("funciones.php");
//Controlar los errores en la ejecucion
controlErrores();

//inicializo la variable 
$salida="";
//compruebo el usuario logueado
if (!isset($_SESSION["usuario"])) {
    header("Location: index.php");
}else{
    //cargo el menu
    $menu = menu([
        "Inicio" => "index.php",
        "Mensajes" => "mensaje.php",
        "Salir" => "salir.php"
    ]);

    $conexion = new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);
    //comprobamos la conexion
    if ($conexion->connect_error) {
        die("Error de conexión: " . $conexion->connect_error);
    }

    //preparo la consulta
    $sql = "SELECT * FROM mensajes";
    //obtengo el mysqli_result
    $resultados = $conexion->query($sql);

    $salida = gridViewBotones($resultados, [
        "Borrar" => "borrar.php"
    ]);

    if ($_POST) {
        //leer los datos del formulario
        $datos["mensaje"] = $_POST["mensaje"];

        //creo la fecha
        $fecha = date("Y-m-d H:i:s");
        //preparo la consulta
        $sql = "INSERT INTO mensajes (nombre,mensaje) VALUES ('{$_SESSION["usuario"]}','{$datos["mensaje"]}')";
        if ($conexion->query($sql)) {
            $aviso = "Mensaje añadido";
        }else{
            $aviso = "Error al insertar";
        }
    }

    // preparar el texto de la consulta
    $sql = "SELECT * FROM mensaje";

    // obtengo el mysqli_result
    $resultados = $conexion->query($sql);

    $salida = gridViewBotones($resultados, [
        "borrar" => "borrar.php"
    ]);



}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1></h1>
    <div>
        <?= $menu ?>
    </div>
    <div>
        <?php
        require '_form.php';
        ?>
    </div>
    <div>
        <?= $salida ?>
    </div>
</body>
</html>