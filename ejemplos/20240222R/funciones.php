<?php
    function controlErrores(){
        //desactivar errores en pantalla
        //error_reporting(0);
        // convertir los errroes sde la libreria mysqli en warnings
        mysqli_report(MYSQLI_REPORT_OFF);
    }

    //funcion que crea la tabla
    function gridViewBotones(mysqli_result $resultados, array $botones): string
{
    if ($resultados->num_rows > 0) {
        $registros = $resultados->fetch_all(MYSQLI_ASSOC);
        // mostrar los registros
        $salida = "<table border='1'>";
        $salida .= "<thead><tr>";
        $campos = array_keys($registros[0]);
        foreach ($campos as $campo) {
            $salida .= "<td>$campo</td>";
        }
        // añado una columna para los botones
        $salida .= "<td>Acciones</td>";
        // foreach($registros[0] as $campo=>$valor){
        //     $salida .= "<td>$campo</td>";
        // }
        $salida .= "</tr></thead>";
        // muestro todos los registros
        foreach ($registros as $registro) {
            $salida .= "<tr>";
            // mostrando los campos
            foreach ($registro as $campo => $valor) {
                $salida .= "<td>" . $valor . "</td>";
            }
            // mostrando los botones
            $salida .= "<td>";

            foreach ($botones as $label => $enlace) {
                $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> | ";
            }

            // cerramos la celda
            $salida .= "</td>";

            $salida .= "</tr>";
        }
        $salida .= "</table>";
    } else {
        $salida = "No hay registros";
    }
    return $salida;
}


    function menu(array $elemento):string{
        $salida = "";
        $salida .= "<ul>";
        foreach ($elemento as $label => $enlace) {
         # code...
         $salida .= "<li><a href='{$enlace}'>{$label}</a></li>";
        }
        $salida .= "</ul>";
        return $salida;
    }
