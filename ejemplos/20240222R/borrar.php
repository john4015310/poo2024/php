<?php
session_start();
$parametros = require_once("parametros.php");
require_once("funciones.php");
//Controlar los errores en la ejecucion
controlErrores();

//inicializo la variable 
$conexion = new mysqli($parametros["bd"]["servidor"], $parametros["bd"]["usuario"], $parametros["bd"]["password"], $parametros["bd"]["nombreBd"]);
    //comprobamos la conexion
    if ($conexion->connect_error) {
        die("Error de conexión: " . $conexion->connect_error);
    }


    //compruebo si esta logueado
if(isset($_SESSION["nombre"])){
    //preparo la consulta para eliminar el mensaje
    if(isset($_GET["id"])){
        $sql = "SELECT * FROM mensajes WHERE id = {$_GET["id"]}";
        $conexion->query($sql);
    }
        //reirijo a la pagina de mensajes
        header("Location: mensaje.php");
    
}else{
    //reirijo a la pagina de login
    header("Location: index.php");
}

