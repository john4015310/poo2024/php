<?php
//Iniciar la sesion
session_start();
$salida = "";
//parametros
$parametros = require_once("parametros.php");
//cargo funciones
require_once("funciones.php");
    //control de errores
    controlErrores();
    
    //compruebo si he pulsado el boton de login
    if ($_POST) {
        //creo una variable de session para comprobar que estoy logeado
        $_SESSION["nombre"] = $_POST["nombre"];
    }

    //compruebo si he pulsado el boton de login
    if(isset($_SESSION["nombre"])) {
        //cargo el menu
        $menu = menu([
            "Inicio" => "index.php",
            "Mensajes" => "mensaje.php",
            "Salir" => "salir.php"
        ]);
        $salida = $menu;
        $salida .= "Bienvenido {$_SESSION["nombre"]}";
    }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1><?= $parametros["aplicacion"]["nombreAplicacion"] ?></h1>
    <?php
        if(!isset($_SESSION["nombre"])) {
            require_once("_login.php");
        }
    ?>

    <?= $salida ?>
</body>
</html>