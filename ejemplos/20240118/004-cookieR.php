<?php
// Crear un formulario que me permita introducir un numero
// almacenar ese numero en una cookie y mostrarlo por pantalla
if($_POST){

    setcookie('cnumero', $_POST['numero'], time() + 3600);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<div>
    <h1>Formulario</h1>   
    <form  method="post">
        <div>
            <label for="numero"></label>
            <input type="number" name="numero" id="numero">
        </div>
        <input type="submit" value="Enviar">
    </form>
    <h2>dato actual</h2>
    <p> <?php echo $_POST['numero']?? '';?> </p>

    <h2>dato anterior</h2>
    <p> <?php echo $_COOKIE['cnumero'] ?? 'no hay cookie'; ?> </p>
    
</div>
</body>

</html>