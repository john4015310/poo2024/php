<?php
//me carga la clase Empleado en el espacio actual
use src\Empleado;
use src\Cliente;
use src\Directivo;
session_start();
//carga automatica de clases
require "autoload.php";
//para poder utilizar sessiones
    
    // he pulsado el boton de enviar
    if (isset($_POST["empleado"])) {
        //tengo que pasar los datos del array POST (asociativo) a un objeto del tipo empleado
        $empleado = new Empleado();
        $empleado->asignar($_POST);
        //var_dump($empleado);
        
    }    
    if(isset($_POST["cliente"])){
        $cliente = new Cliente();
        $cliente->asignar($_POST);

    }

    if(isset($_POST["directivo"])){
        $directivo = new Directivo();
        $directivo->asignar($_POST);

    }
    //quiero que me muestr todos los registros introducios en la session actual


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    //carga el fichero _empleado.php
    require "_empleado.php";
    require "_cliente.php";
    require "_directivo.php";
    //si he rellenado el formulario de empleado anteriormente
    if (isset($empleado)) {
        echo $empleado;//llamo al metodo toString

        $_SESSION["empleados"][] = serialize($empleado);

    }
    if (isset($cliente)) {
        echo $cliente;//llamo al metodo toString
        $_SESSION["clientes"][] = serialize($cliente);
    }
    //------------------------------------------------------------------------
    //muestro los registros
    echo "<hr>";
    echo "<h2>Empleados</h2>";
    if (isset($_SESSION["empleados"])) {
        foreach ($_SESSION["empleados"] as $empleado) {
            $empleado = unserialize($empleado);
            echo $empleado;
        }
    }else{
        echo "No hay empleos";
    }
    
    echo "<h2>Clientes</h2>";
    if(isset($_SESSION["clientes"])){
        foreach ($_SESSION["clientes"] as $cliente) {
            $cliente = unserialize($cliente);
            echo $cliente;
        }
    }else{
        echo "No hay clientes";
    }
    
    echo "<h2>Directivo</h2>";
    if(isset($_SESSION["directivo"])){
        foreach ($_SESSION["directivo"] as $cliente) {
            $cliente = unserialize($cliente);
            echo $cliente;
            var_dump($cliente);
        }
    }else{
        echo "No hay directivos";
    }
    ?>
</body>

</html>
