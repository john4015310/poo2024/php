<?php

namespace src;

class Directivo extends Empleado
{
    protected ?string $categoria;

    // sobreescribir metodo mostrar
    public function mostrar(): string
    {
        $salida = "<ul>";
        $salida = "<li>Nombre: " . $this->getNombre() . "</li>";
        $salida .= "<li>Edad: " . $this->getEdad() . "</li>";
        $salida .= "<li>Sueldo: " . $this->getSueldo() . "</li>";
        $salida .= "<li>Categoria: " . $this->categoria . "</li>";
        $salida .= "</ul>";
        return $salida;
    }



    /**
     * Get the value of categoria
     *
     * @return ?string
     */
    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @param ?string $categoria
     *
     * @return self
     */
    public function setCategoria(?string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    
    //construtor
    public function __construct(){
        parent::__construct();
        $this->setCategoria(null);
        //en el constructor indico que campos quiero que se asigne automticamente desde un formlulario
        $this->propiedadesAsignacionMaxiva = ["nombre","edad","categoria"];
    }
}

