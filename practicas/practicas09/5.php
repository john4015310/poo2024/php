<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="ejercicio5Destino.php"> 
        <div>
            <label for="nombre">
                Nombre:
            </label>
            <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" required>
        </div>
        <br>
        <div>
            <label for="apellidos">
                Apellidos:
            </label>
            <input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos" required>
        </div>
        <br>
        <div>
            <label for="direccion">
                Direccion:
            </label>
            <input type="text" name="direccion" id="direccion" placeholder="Introduce tu direccion" required>
        </div>
        <br>
        <div>
            <label for="codigoP">
                Codigo postal:
            </label>
            <input type="text" name="codigoP" id="codigoP" placeholder="Introduce tu codigo postal" required>
        </div>
        <br>
        <div>
            <label for="Telefono">
                Telefono:
            </label>
            <input type="text" name="telefono" id="telefono" placeholder="Introduce tu Telefono" required>
        </div>
        <br>
        <div>
            <label for="email">
                Email:
            </label>
            <input type="email" name="email" id="email" placeholder="Introduce tu email" required>
        </div>
        <br>
        <div>
            <button >Enviar</button>
            
        </div>
    </form>
</body>
</html>