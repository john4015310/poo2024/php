<?php
namespace clases;
include '..\autoload.php';

class Cadena{
    private $valor;
    private $longitud;
    private $vocales;
    //constructor
    public function __construct($valor){
        $this->valor = $valor;
        
    }
     //getter y setter
    public function getValor($minuscula = false){
        if($minuscula){
            return strtolower($this->valor);
        }else{
            return $this->valor;
        }

    }

    public function getLongitud(){
        return strlen($this->valor);
    }

    

    //metodos

    public function calcularLongitud(){
        $this->longitud = strlen($this->valor);
        return $this->longitud;
    }

    public function numeroVocales(){
        $this->vocales = substr_count($this->valor, 'a') + substr_count($this->valor, 'e') + substr_count($this->valor, 'i') + substr_count($this->valor, 'o') + substr_count($this->valor, 'u');
        return $this->vocales;
        
    }
}