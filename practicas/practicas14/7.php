<?php

function mostrar(){
    foreach(func_get_args() as $valor){
        echo "<br />$valor<br />";
    }
}

/**
 * Vamos analizar la funcion strtotime
 */

$fecha = "2000/12/25";

mostrar(date("d/m/y", strtotime($fecha)));


mostrar(date("d/m/y", strtotime("now")));


mostrar(date("d/m/y", strtotime("+1 day")));


mostrar(date("d/m/y", strtotime("+1 day", strtotime($fecha))));


mostrar(date("d/m/y", strtotime("previous Monday")));
echo "==================================================";
$fecha = "10/12/2012";
$fecha = explode("/", $fecha);
$fecha = implode("/", array_reverse($fecha));
mostrar(date("d/m/Y", strtotime($fecha)));

var_dump($fecha);

