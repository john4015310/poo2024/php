<?php

    function mostrar(){
        static $c = 0;
        foreach(func_get_args() as $valor){
            echo "<br>$c- $valor<br>";
            $c++;
        }
    }

    /**
     * Analicemos las funciones date() y time()
     */

     $fechaActual = time();

     //echo mostrar(date('Y-m-d', $fechaActual), date('H:i:s', $fechaActual), date('d-m-Y', $fechaActual), date('l', $fechaActual), date('d-m-Y H:i:s', $fechaActual), date('d-m-Y H:i:s', $fechaActual));

     mostrar("Raro, raro:", $fechaActual);
     mostrar(date("d/m/y",  $fechaActual));

     mostrar(date("d/m/y"));
     mostrar(date("j"), date("d"), date("D"));
     mostrar(date("N"), date("F"), date("m"));
     mostrar(date("Y"), date("y"));

     //aqui el valor que recibimos el es la fecha actual
     //con los diferente formatos
    // que nos va a devolver
    //d-m-Y H:i:s 
