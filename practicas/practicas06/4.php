<?php
/**Hacer que cada vez que cargue una página web debe colocar un numero entero entre 1 y 10.
Para ello debéis utilizar la función mt_rand.
Si el numero es par debe indicarlo con el mensaje "el número es par" */

$num=mt_rand(1,10);
echo "El numero aleatorio es: {$num}<br>";
if($num%2==0){
    echo "El Número es par";
}else{
    echo "El número es impar";
}