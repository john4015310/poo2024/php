<?php
/**
 * Elabore un programa que coloque dos números en dos variables denominadas a y b. Posteriormente realice las
    *siguientes operaciones con ellos:
            *- Suma
            *- Resta
            *- multiplicación
            *- división
            *- raíz cuadrada del primero
            *- el primer número elevado al segundo
    *Muestre el resultado por pantalla
 */

 $numero1=20;
 $numero2=5;

 $suma= $numero1+ $numero2;
 $resta= $numero1 - $numero2;
 $producto=$numero1 * $numero2;
 $division= $numero1 / $numero2;
 $raiz= sqrt($numero1);
 $potencia = pow($numero1, $numero2);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <div>
        <ul>
            <li>Suma : <?= $suma ?></li>
            <li>Resta: <?= $resta ?></li>
            <li>Multiplicacion: <?= $producto ?></li>
            <li>División: <?= $division ?></li>
            <li>Raíz cuadrada: <?= $raiz ?></li>
            <li>Potencia : <?= $potencia ?> </li>
        </ul>
    </div>
</body>
</html>