<?php
/**Realice un programa que cree una variable denominada dia y colocarle un numero entero directamente.
Debe indicarnos el mes correspondiente en texto. En caso de que no sea ningún mes (1 al 12) debe indicar que no
es un mes correcto */

$dia=12;
$meses=[" ", "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

if($dia>0 && $dia<13){
    echo $meses[$dia];
}else{
    echo "Numero mes incorrecto";
}
