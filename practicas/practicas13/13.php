<?php
    $colores = array("red", "green", "blue");
    function colorear(array $datos):string{
        $imprimir = "";
        foreach($datos as $color){
            $imprimir .= "<div style='background-color: $color;'>$color</div>";
        }
        
        return $imprimir;
    }

    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        div{
            height: 100px;
            color: white;
            text-align: center;
            font-size: larger;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <?=colorear($colores)?>
</body>
</html>