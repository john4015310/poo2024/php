<?php

    $a=[1,2,3];
    $b=[4,5,6];
    $c=[7,8,9];

    function sumarArray(array $a, $b, $c):int{
        $suma = 0;
        for($i = 0; $i < 3; $i++){
            $suma += $a[$i] + $b[$i] + $c[$i];
        }
        return $suma;
    }

    echo sumarArray($a, $b, $c);