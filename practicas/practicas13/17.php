<?php

    $palabra = "ejemplo de clase";
    
    function numeroVocal($word): int{
        $separar = str_split($word);
        $contador = 0;
        foreach ($separar as $key => $value) {
            # code...
            switch ($value) {
                case 'a':
                    # code...
                    $contador++;
                    break;
                case 'e':
                    # code...
                    $contador++;
                    break;
                case 'i':
                    # code...
                    $contador++;
                    break;
                case 'o':
                    # code...
                    $contador++;
                    break;
                case 'u':
                    # code...
                    $contador++;
                    break;
                
                default:
                    
                    break;
            }
        }

        return $contador;
    }

    echo "la palabra '$palabra' contiene ".numeroVocal($palabra)." vocales";