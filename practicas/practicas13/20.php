<?php

    $numero1 = 25;
    $numero2 = 16;

    function elegirOperacion($num1, $num2, $operacion): int{
        $resultado = 0;
        switch ($operacion) {
            case 'sumar':
                $resultado = $num1 + $num2;
                break;
            case 'restar':
                $resultado = $num1 - $num2;
                break;
            case 'multiplicar':
                $resultado = $num1 * $num2;
                break;
            default:
                # code...
                echo "la operacion '$operacion' no existe";
                break;
        }

        return $resultado;
    }


    echo elegirOperacion($numero1, $numero2, 'multiplicar');