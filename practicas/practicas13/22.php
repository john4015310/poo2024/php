<?php

    $letras1 = ["a", "b", "c" , "d", "e" , "f" , "g"];
    $letras2 = ["h", "b", "j" , "d", "e" , "m" , "g"];

    function interseccion($letras1, $letras2): int{
        $coincidencias = 0;
        foreach ($letras1 as $key => $value) {
            if(in_array($value, $letras2)){
                $coincidencias++;
            }
        }

        return $coincidencias;
    }
    

    echo interseccion($letras1, $letras2);