<?php
    $numeros = [1,2,3,4,5,6,7,8,9,10];
    function sumarN(array $dato):int{
        $sum = 0;
        foreach ($dato as $value) {
            $sum += $value;
        }
        
        return $sum;
    }

    echo sumarN($numeros);