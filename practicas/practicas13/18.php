<?php

    $palabra = "ejemplo de clase";

    function convertirEnArray($word): array{
        $separar = str_split($word);
        $resultado = [];

        foreach ($separar as $key => $value) {
            switch ($value) {
                case 'a':
                    
                    $resultado[] = "a";
                    break;
                case 'e':
                    $resultado[]="e";
                    break;
                case 'i':
                    $resultado[]="i";
                    break;
                case 'o':
                    $resultado[]="o";
                    break;
                case 'u':
                    $resultado[]="u";
                    break;
                default:
                    # code...
                    break;
            }
        }
        return $resultado;
    }

    var_dump(convertirEnArray($palabra));