<?php
namespace ejercicio02\clases;

use ejercicio01\clases\Electrodomesticos;

class Lavadora extends Electrodomesticos{
    public float $precio;
    public bool $aguaCaliente;

    //La clase Lavadora tendrá un constructor que recibirá como parámetros la marca de la lavadora, su precio, su
    //potencia y su modo de funcionamiento (agua fría o caliente)

    
    public function __construct(string $marca, float $potencia, float $precio, bool $aguaCaliente){
        $this->marca = $marca;
        $this->potencia = $potencia;
        $this->precio = $precio;
        $this->aguaCaliente = $aguaCaliente;
        
    }

    public function __toString():string{
        $mostrar = "Tipo: " . $this->marca . "<br> Potencia: " . $this->potencia . "<br> Precio: " . $this->precio . "<br> Modo de funcionamiento: " . $this->aguaCaliente;
        return $mostrar;
    }

    public function getConsumo(int $horas):float{
        $aguaCaliente = $this->aguaCaliente;
        if($aguaCaliente == true){
            $consumo = $horas * $this->potencia;
            return $consumo;
        }else{
            $consumo = $this->potencia + $this->potencia * 0.20 ;
            return $consumo;

        }
    }

    
}