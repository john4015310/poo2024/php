<?php

namespace ejercicio01\clases;

class Electrodomesticos{
    public string $tipo;
    public string $marca;
    public float $potencia;

    public function __construct(string $tipo, string $marca, float $potencia){
        $this->tipo = $tipo;
        $this->marca = $marca;
        $this->potencia = $potencia;
    }

    public function __toString():string{
        $mostrar = "Tipo: " . $this->tipo . "<br> Marca: " . $this->marca . "<br> Potencia: " . $this->potencia;
        return $mostrar;
    }

    //Setters y getters
    public function getTipo():string{
        return $this->tipo;
    }

    public function setTipo(string $tipo):self{
        $this->tipo = $tipo;
        return $this;
    }

    public function getMarca():string{
        return $this->marca;
    }

    public function setMarca(string $marca):self{
        $this->marca = $marca;
        return $this;
    }

    public function getPotencia(){
        return $this->potencia;
    }

    public function setPotencia(float $potencia):self{
        $this->potencia = $potencia;
        return $this;
    }

    //metodos

    public function getConsumo(int $horas):float{
        $consumo = $horas * $this->potencia;
        return $consumo;
        
    }

    public function getCosteConsumo(int $horas, float $costeHora):float{
        $costeTotal = $horas * $costeHora;
        return $costeTotal;
        
    }


}