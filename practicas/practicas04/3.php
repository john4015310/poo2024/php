<?php

//Crear una pagina nueva y escribir el siguiente codigo
$nombre="Ramon";

$poblacion="Santander";

//Mostrar las dos variables en dos divs utilizano la impresion de php contraida

echo "<div>";
echo $nombre;
echo "</div>";
echo "<div>";
echo $poblacion;
echo "</div>";

//solucion 2

echo "<div>" . $nombre . "</div>";
echo "<div>" . $poblacion . "</div>";

//solucion 3

echo "<div> $nombre </div>";
echo "<div> $poblacion </div>";

//Solucion 4

echo "<div> {$nombre} </div>";
echo "<div> {$poblacion} </div>";

//Solucion 5
//utilizando heredoc
$salida=<<<"BLOQUE"
<div> {$nombre} </div>
<div> {$poblacion} </div>
BLOQUE;

echo $salida;

//Solucion 6

$salida=<<<BLOQUE
<div> {$nombre} </div>
<div> {$poblacion} </div>
BLOQUE;

echo $salida;




?>

