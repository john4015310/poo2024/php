<?php
    /**
     * Crear un array con las vocales (introducir los valores a mano).
     * Mostrar el contenido de la variable utilizando la función var_dump.
     * Una posible solución
     */

     $arrayVocales=["a","e","i","o","u"];

     var_dump($arrayVocales);

    $vocales=array("a","e","i","o","u");

    var_dump($vocales);
?>