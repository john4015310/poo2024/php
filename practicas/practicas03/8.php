<?php

$datos=[
    [
        "nombre"=> "Eva",
        "edad"=> 50
    ],
    [
        "nombre"=> "Jose",
        "edad"=> 40,
        "peso"=> 80
    ]
];

//introducri a Lorena de 80 años y con una altura de 175
//realizarlo directamente

$datos[]=[
    "nombre"=> "Lorena",
    "altura"=> 175
];



// introducir a "luis" de 20 años y con un peso de 90 y a Oscar de 23 años.
//realizarlo mediante una funcion push

array_push($datos,[
    "nombre"=> "Luis",
    "edad"=> 20,
    "peso"=> 90
],
[
    "nombre"=> "Oscar",
    "edad"=> 23
]);

var_dump($datos);