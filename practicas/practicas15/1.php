<?php
//Complete la función para que encuentre la media de las tres puntuaciones que se le pasaron y devuelva el
//valor de la letra asociada con esa calificación.

function notas($a,$b,$c){

    $media = ($a+$b+$c)/3;
    if($media<60){
        return "F";
    }elseif($media<70){
        return "D";
    }elseif($media<80){
        return "C";
    }elseif($media<90){
        return "B";
    }else{
        return "A";

    }
}

function notas1(...$a){

    $media = array_sum($a)/count($a);
    if($media<60){
        return "F";
    }elseif($media<70){
        return "D";
    }elseif($media<80){
        return "C";
    }elseif($media<90){
        return "B";
    }else{
        return "A";
    }
}

echo notas1(50,80,80);