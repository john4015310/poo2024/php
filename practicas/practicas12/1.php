<?php
    $width=140;
    $height= 150;
    $tiradas=[];
    $suma=0;

    //procesos
    
    function ejecutar(){
        $tirada1 = mt_rand(1,6);
        $tirada2 = mt_rand(1,6);
        $tiradas=[$tirada1,$tirada2];
        return $tiradas;
    }
    
    $tiradas = ejecutar();
    $suma=$tiradas[0]+$tiradas[1];
    
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Tiradas</title>
</head>
<body>
    <div id="box">
        <div class="t1">
            <img src="dados/<?=$tiradas[0]?>.svg" alt="" height="<?=$height?>px" width="<?=$width?>px">
            <img src="dados/<?=$tiradas[1]?>.svg" alt="" height="<?=$height?>px" width="<?=$width?>px">

        </div>
        <div class="t2">
            <label>Total:  </label>
            <div class="imp">
                <label><?=$suma?></label>
            </div>
        </div>
    </div>
</body>
</html>