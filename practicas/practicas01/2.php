<!-- Quiero que desarrolléis una página Web que contenga la siguiente tabla. Esta página tiene que tener las
siguientes características:

    La tabla estará realizada utilizando únicamente HTML
    El contenido de la tabla estará realizado con php
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>
<body>
    <h1>Ejercicio 2 de la practica 1</h1>

    <table width=100% border=2px solid black>
        <tr>
            <td> <?php echo "Este texto esta escrito utilizando la funcionecho de PHP"; ?> </td>
            <td>Este texto esta escrito en HTML</td>
        </tr>
        <tr>
            <td><?php print("Este texto esta escrito en HTML"); ?></td>
            <td>Centro de Formacion Alpe</td>
        </tr>
    </table>

</body>
</html>