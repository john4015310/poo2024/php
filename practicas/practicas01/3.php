<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=
    , initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>

<h2>Metodo 1</h2>
<p>
    <?php
        //mezclamos Html y PHP
        echo "<p> Hola mundo </p>";
    ?>
    
    <h2>Metodo 2</h2>
    <p>
        <?php
        //solo coloco php
        echo "Hola Mundo"; 
        
        ?>
    </p>

    <h2>Metodo 3</h2>
    <?php
        echo "<p>";
    ?>
    Hola Mundo
    <?php
        echo "</p>";
    ?>

    <h2>Podemos utilizar el modo contrario del echo</h2>
    <p>
        <?="hola Mundo";?>
    </p>
    
</p>
    
</body>
</html>