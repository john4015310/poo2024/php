<?php
namespace clases;
    class Imagen{

        public string $src;
        private bool $border;
        private ?int $ancho;
        private ?int $alto;
        const RUTA = "imgs/";

        public function __construct(string $src, bool $border, int $ancho=null, int $alto=null){
            $this->src = $src;
            $this->border = $border;
            $this->ancho = $ancho;
            $this->alto = $alto;
        }

        public function __toString(){
            return "<img src=".self::RUTA."{$this->src} border={$this->border} width={$this->ancho} height={$this->alto}>";
            
        }

        public function getAncho(){
            return $this->ancho;
        }

        public function getAlto(){
            return $this->alto;
        }

        public function setAncho(int $ancho){
            $this->ancho = $ancho;
        }

        public function setAlto(int $alto){
            $this->alto = $alto;
        }

    }