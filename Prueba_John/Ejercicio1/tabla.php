<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio - Paso 2</title> 
</head>
<body>
    <fieldset>
        <legend>Ejercicio - Paso 2</legend>
        <form action="resultado.php" method="post">
            <?php
                $num = $_POST["tamaño"];
                $num = (int)$num;
                for ($i = 1; $i <= $num; $i++) {
                    echo "<div><label>{$i}</label> <input type='text' name='tamaño{$i}' id='tamaño{$i}'></div>";
                }
            ?>
            <div class="botones">
                <input type="submit" value="Comprobar">
                <input type="reset" value="Restablecer">
        </div>
        </form>
        
            
    </fieldset>

</body>
</html>