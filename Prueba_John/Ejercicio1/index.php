<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Ejercicio 1</title>
</head>
<body>
    <fieldset>
        <legend>Ejercicio 1</legend>
        <form action="tabla.php" method="post">
            <label for="tamaño">Tamaño de la tabla</label>
            <input type="number" name="tamaño" id="tamaño">
            <div class="botones">
                <input type="submit" value="Dibujar">
                <input type="reset" value="Restablecer">
            </div>

        </form>
    </fieldset>
</body>
</html>