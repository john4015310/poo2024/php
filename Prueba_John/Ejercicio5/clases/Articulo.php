<?php
namespace clases;

class Articulo{
    protected ?string $nombre;
    protected float $precio;

    public function __construct(string $nombre, float $precio=0){
        $this->nombre = $nombre;
        $this->precio = $precio;
    }

    public function __toString(){
        return 'Nombre:' .$this->nombre .'br' . 'Precio:' .$this->precio . '€<br>';
    }

    public function getPrecio(){
        return $this->precio;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre(string $nombre){
        $this->nombre = $nombre;
    }

    public function setPrecio(float $precio){
        $this->precio = $precio;
    }

    
}