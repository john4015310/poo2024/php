<?php

use clases\Imagen;

require_once "autoload.php";

$imagen1 = new Imagen("1.jpg", true);
$imagen2 = new Imagen("foto1.jpg", true,200,200);
$imagen3 = new Imagen("foto3.jpg", false,200,200);
$imagen4 = new Imagen("pic.jpg", true,200,200);

echo $imagen1;
echo $imagen2;
echo $imagen3;
echo $imagen4;

var_dump($imagen1);
var_dump($imagen2);
var_dump($imagen3);
var_dump($imagen4);
